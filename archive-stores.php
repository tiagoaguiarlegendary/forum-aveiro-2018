<?php
/**
 *
 * Archive page for stores post type
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper full-wrapper">

		<div class="<?php echo esc_html( $container ); ?> fullHeight">

			<div class="row fullHeight">

				<div class="col-12 content-area fullHeight" >

					<main class="site-main fullHeight" role="main">

						<div class="row fullHeight">

							<div class="col-12 col-md-2 fullHeight">

								<div class="page-title mb-5">
									<h2 class="body-text-15 ">QUE LOJAS <br>PROCURAS?</h2>
								</div>

								<div class="btn-aqua-blue">
									<a class=" toggleView" >Ver directório</a>
								</div>

								<div class="filter-term filter-genre">
									<?php
										$terms = get_terms( 'genre', array(
												'hide_empty' => true,
												'orderby' => 'name',
										) );
									?>
									<select name="store-genre">
										<option id="genre-all" value="all">Todas</option>

										<?php foreach( $terms as $term ): ?>

											<option value="<?php echo $term->slug?>" data-genre-id="<?php echo $term->term_taxonomy_id;?>"><?php echo $term->name?></option>

										<?php endforeach;?>

									</select>
								</div>

								<div id="results-info-container" class="pos-rel">

									<div class="pos-rel">
										<input autocomplete="off" id="livesearch-input" class="input-search" type="text" name="s" data-action="liveSearch" data-container="#livesearch-container" data-item-class="livesearch-item-result" data-post-type="stores" placeholder="Pesquisar loja">
										<div class="loading-square"></div>
									</div>

									<div id="livesearch-container"></div>

								</div>

							</div>

							<div data-view-container="map" class="col-12 col-md-10">
								<?php echo do_shortcode('[mapplic id="1459"]')?>
							</div>

							<div id="main" data-view-container="directory" class="col-12 col-md-10 scroll-container">
								<div class="row">
									<?php

										$posts_per_page = 24;
										$post_type = 'stores';
										$template_content = 'content-stores';
										$order = 'ASC';
										$orderby = 'title';
										$meta_query_args =  array(
											 array(
												 'key' => 'store_logo',
												 'value' => '',
												 'compare' => '!='
											 )
										);

										$args = array(

											'post_status'       => 'publish',
											'post_type'         => $post_type,
											'order'							=> $order,
											'orderby'						=> $orderby,
											'posts_per_page' 		=> $posts_per_page,
											'meta_query' 				=> $meta_query_args
										);

										args_get_template_part('loop-templates',$template_content,$args);

										?>
									</div>
							</div>

						</div>

					</main>

				</div>

				<div id="loadmore-container" class="col-12 col-md-10 ml-auto content-area text-center">

					<div class="row">

						<div class="col-12">

							<button type="button" onclick="loadmore.get();" class="btn-load-more" name="load-more"></button>

						</div>

					</div>

				</div> <!-- #loadmore-container end -->

			</div><!-- .row end -->

		</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<?php
	$args = array(

		'post_status'       => 'publish',
		'post_type'         => $post_type,
		'posts_per_page' 		=> $posts_per_page

	);

	$the_query = new WP_Query( $args );
	$max_num_pages = $the_query->max_num_pages;

	wp_reset_postdata();
?>

<script>
	var map,map_data,activeView = 'map';

	$(function (){

		//Map variable
		map = $('.mapplic-element');
		map_data = map.data('mapplic');

		map.on('locationopened', function(e, location) {
			// location grants full access to the location
			search.store(location.id,'store_id');
		});

		liveSearch.init('#livesearch-input');

		loadmore.init({
			post_type : '<?php echo $post_type?>',
			order : '<?php echo $order?>',
			orderby : '<?php echo $orderby?>',
			posts_per_page : '<?php echo $posts_per_page?>',
			max_num_pages : '<?php echo $max_num_pages ?>',
			meta_query_args: '<?php echo json_encode($meta_query_args, JSON_FORCE_OBJECT)?>'
		}, '<?php echo $template_content?>','#main', true);

		filter.init({
			post_type : '<?php echo $post_type?>',
			posts_per_page : '<?php echo $posts_per_page?>',
			max_num_pages : '<?php echo $max_num_pages ?>',
			taxonomy : 'genre',
			field : 'slug',
			action : 'filter_store_genre'
		}, '.filter-term select');

		$('.toggleView').on('click',function (){
			var $map_container = $('div[data-view-container="map"]'),
				$directory_container = $('div[data-view-container="directory"]'),
				$loadmore = $('#loadmore-container'),
				$filter = $('.filter-term');

			if (activeView == 'map'){
				activeView = 'directory';
				fullContainerPage('.scroll-container',false, true);
				$(this).html('Ver mapa');
				$map_container.finish().hide("slide", { direction: "left" }, 400, 'easeInOutQuad');
				$directory_container.delay(450).show("slide", { direction: "right" }, 400, 'easeInOutQuad');
				$loadmore.delay(450).fadeIn();
				$filter.delay(450).fadeIn();
			}
			else{
				activeView = 'map';
				fullContainerPage('.scroll-container',false, false);
				$(this).html('Ver directório');
				$map_container.delay(450).show("slide", { direction: "left" }, 400, 'easeInOutQuad');
				$directory_container.finish().hide("slide", { direction: "right" }, 400, 'easeInOutQuad');
				$('.item-store.active').find('.item-store-container').fadeOut(function (){
					$(this).parent('.item-store').removeClass('active').find('.item-store-logo').fadeIn();
				});
				$loadmore.fadeOut();
				$filter.fadeOut();
			}
		});

		$('.post-type-archive-stores').on('click','.item-store:not(".white-info")',function (){
			var that = $(this);

			if (!$(this).hasClass('active')) {
				$('.item-store.active').find('.item-store-container').fadeOut(function (){
					$(this).parent('.item-store').removeClass('active').find('.item-store-logo').fadeIn();
				});

				that.find('.item-store-logo').fadeOut(function (){
					that.addClass('active').find('.item-store-container').fadeIn();

					$('.scroll-container').animate({
						scrollTop: that[0].offsetParent.offsetTop
					}, 750, 'swing');

				})
			}
		});

		$('.post-type-archive-stores').on('click','.item-store-info-dismiss',function (){
			var that = $(this).parents('.item-store');

			if (that.hasClass('active')) {
				that.find('.item-store-container').fadeOut(function (){
					that.removeClass('active').find('.item-store-logo').fadeIn();
				});
			}
		});

	});


</script>
