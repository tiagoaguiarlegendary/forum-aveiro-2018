# Installation

## Classic install

* cd to wordpress theme folder
* git clone https://legendNunoC@bitbucket.org/LegendaryPeople/alma.git
* Make sure you have installed [Node.js](http://nodejs.org)
* Run: `$ npm install` or `npm install` on windows

## WordPress.org install
* Open your WordPress backend
* Click on "Appearance -> Themes"
* Activate the alma theme

# Page Templates

## Blank Template

The `blank.php` template is useful when working with various page builders and can be used as a starting blank canvas. It will display what you put on the content of the post.

## Blank Form Template

The `blank-form.php` template is used to add a page with a form. It can also have a button.

## Blank Full Background Template

The `blank-full-bg.php` template is like the blank template, but the container will occupy the full window. This page has a background image by default and on this development it can only be change on the css.

## Blank Scroll Container Template

The `blank-scroll-container.php` template display the content inside a container with scroll.

## Brand Kit, Center, Homepage, Jobs, Oportunities, Services and Tourist Info Page Template

This are custom page templates used on the respective page.

# Development 

## File Structure

```
alma/
|—— css/
|   |—— bower_components/
|   |   |—— # css bower modules/librarie compiled file 
|   |—— # css theme compiled files
|—— global-templates/
|   |—— blank.php
|   |—— blank-form.php
|   |—— blank-full-bg.php
|   |—— blank-scroll-container.php
|—— img/
|   |—— src/
|   |   |—— # all images files
|   |—— # all minify images files
|—— inc/
|   |—— # php includes to functions.php
|—— js/
|   |—— bower_components/
|   |   |—— # js bower modules/librarie compiled file 
|   |—— # js theme compiled files
|—— loop-templates/
|   |—— # php loop templates for theme componentes
|—— page-templates/
|   |—— brand-kit.php
|   |—— center.php
|   |—— homepage.php
|   |—— jobs.php
|   |—— oportunities.php
|   |—— services.php
|   |—— tourist-info.php
|—— sass/
|   |—— assets/
|   |   |—— bootstrap4.scss
|   |   |—— font-awesome.scss
|   |   |—— underscores.scss
|   |—— theme/
|   |   |—— components/
|   |   |   |—— # sass files related to the theme components like buttons, form etc
|   |   |—— pages/
|   |   |   |—— # sass files related to the theme pages like templates, post type archive etc
|   |   |—— _contact-form7.scss
|   |   |—— _fonts.scss
|   |   |—— _media-queries.scss
|   |   |—— _mixin-library.scss
|   |   |—— _reset.scss
|   |   |—— _theme.scss
|   |   |—— _theme_variables.scss
|   |—— understrap/
|   |   |—— understrap.scss
|   |—— theme.scss
|—— svg/
|   |—— src/
|   |   |—— # all svg files
|   |—— # all minify svg files
|—— .gitignore
|—— # php wordpress files such archive pages, single, functions, styles.css etc
|—— CHANGELOG.md
|—— codesniffer.ruleset.xml
|—— gulpfile.js
|—— package.json
|—— README.md
|—— readme.txt
```

## Build commands

* To start the development
```shell
gulp watch
```

* To build a development package
```shell
gulp dist-dev
```
* To deploy on dev server
```shell
gulp deploy-dev
```

* To build a production package
```shell
gulp dist
```
* To deploy on dev server
```shell
gulp deploy
```

* After a bower install you should run
```shell
gulp bower
```