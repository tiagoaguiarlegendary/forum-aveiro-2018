// Defining base pathes
const basePaths = {
	bower: './bower_components/',
	node: './node_modules/',
	dev: './src/',
	bower_js: './js/bower_components/',
	bower_css: './css/bower_components/'
};


// browser-sync watched files
// automatically reloads the page when files changed
const browserSyncWatchFiles = [
    './css/*.min.css',
    './js/*.min.js',
    './**/*.php'
];


// browser-sync options
// see: https://www.browsersync.io/docs/options/
const browserSyncOptions = {
	proxy: "localhost/wordpress/",
	notify: false
};


// Defining requirements
// Defining requirements
const gulp = require('gulp'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	ignore = require('gulp-ignore'),
	rimraf = require('gulp-rimraf'),
	clone = require('gulp-clone'),
	merge = require('gulp-merge'),
	browserSync = require('browser-sync').create(),
	del = require('del'),
	gulpSequence = require('gulp-sequence'),
	ftp = require('vinyl-ftp');

//SASS, CSS and JS operation
const cleanCSS = require('gulp-clean-css'),
	cssnano = require('gulp-cssnano'),
	uglify = require('gulp-uglify'),
	merge2 = require('merge2'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps');

//Images operation
const svgmin = require('gulp-svgmin'),
	imagemin = require('gulp-imagemin');

//Bower requirements
const wiredep = require('wiredep'),
	inject = require('gulp-inject');


// Run:
// gulp sass + cssnano + rename
// Prepare the min.css for production (with 2 pipes to be sure that "theme.css" == "theme.min.css")
gulp.task('scss-for-prod', function () {
	const source = gulp.src('./sass/*.scss')
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(sass());

	const pipe1 = source.pipe(clone())
		.pipe(sourcemaps.write(undefined, {
			sourceRoot: null
		}))
		.pipe(gulp.dest('./css'))
		.pipe(rename('custom-editor-style.css'))
		.pipe(gulp.dest('./css')); // add this


	const pipe2 = source.pipe(clone())
		.pipe(cssnano({
			discardComments: {
				removeAll: true
			}
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./css'));

	return merge(pipe1, pipe2);
});


// Run:
// gulp sourcemaps + sass + reload(browserSync)
// Prepare the child-theme.css for the development environment
gulp.task('scss-for-dev', function () {
	gulp.src('./sass/*.scss')
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(sass())
		.pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
		.pipe(sourcemaps.write(undefined, {
			sourceRoot: null
		}))
		.pipe(gulp.dest('./css'))
});

gulp.task('watch-scss', ['browser-sync'], function () {
	gulp.watch('./sass/**/*.scss', ['scss-for-dev']);
});


// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function () {
	const stream = gulp.src('./sass/*.scss')
		.pipe(plumber())
		.pipe(sourcemaps.init()) // add this
		.pipe(sass())
		.pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
		.pipe(sourcemaps.write('./', {
			includeContent: false
		})) // add this
		.pipe(sourcemaps.init({
			loadMaps: true
		})) // add this
		.pipe(gulp.dest('./css'))
		.pipe(rename('custom-editor-style.css'))
		.pipe(gulp.dest('./css')); // add this
	return stream;
});


// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task('watch', function () {
	gulp.watch('sass/**/*.scss', ['styles']);
	gulp.watch([basePaths.dev + 'js/**/*.js', '!js/theme.js', '!js/theme.min.js'], ['scripts']);

	//Inside the watch task.
	gulp.watch('img/src/**', ['imagemin']);
	gulp.watch('svg/src/**', ['svgmin']);
});

// Run:
// gulp imagemin
// Running image optimizing task
gulp.task('imagemin', function () {
	gulp.src('img/src/**')
		.pipe(imagemin([
        imagemin.gifsicle({
				interlaced: true
			}),
        imagemin.jpegtran({
				progressive: true
			}),
        imagemin.optipng({
				optimizationLevel: 5
			}),
        imagemin.svgo({
				plugins: [{
					removeViewBox: true
				}]
			})
    ]))
		.pipe(gulp.dest('img'))
});

// Run:
// gulp imagemin
// Running image optimizing task
gulp.task('svgmin', function () {
	gulp.src('svg/src/**')
		.pipe(svgmin())
		.pipe(gulp.dest('svg'));
});


// Run:
// gulp cssnano
// Minifies CSS files
gulp.task('cssnano', function () {
	return gulp.src('./css/theme.css')
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(cssnano({
			discardComments: {
				removeAll: true
			}
		}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./css/'))
});

gulp.task('minify-css', function () {
	return gulp.src('./css/*.css')
		.pipe(ignore('*.min.css'))
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(cleanCSS({
			compatibility: '*'
		}))
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./css/'));
});

gulp.task('cleancss', function () {
	return gulp.src('./css/*.min.css', {
			read: false
		}) // much faster
		.pipe(ignore('*.css'))
		.pipe(rimraf());
});

gulp.task('styles', function (callback) {
	gulpSequence('sass', 'minify-css')(callback)
});


// Run:
// gulp browser-sync
// Starts browser-sync task for starting the server.
gulp.task('browser-sync', function () {
	browserSync.init(browserSyncWatchFiles, browserSyncOptions);
});


// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch-bs', ['browser-sync', 'watch', 'scripts'], function () {});


// Run:
// gulp scripts.
// Uglifies and concat all JS files into one

gulp.task('scripts', function () {
	const scripts = [
        basePaths.dev + 'js/tether.js', // Must be loaded before BS4

        // Start - All BS4 stuff
        basePaths.dev + 'js/bootstrap4/bootstrap.js',

        // End - All BS4 stuff
        basePaths.dev + 'js/skip-link-focus-fix.js',

        // All scripts
        basePaths.dev + 'js/theme-scripts/*.js',
    ];

	gulp.src(scripts)
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(concat('theme.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./js/'));

	gulp.src(scripts)
		.pipe(concat('theme.js'))
		.pipe(gulp.dest('./js/'));
});


gulp.task('bower', function () {
	const target = gulp.src(['./header.php', './footer.php']);

	const js = gulp.src(wiredep().js);
	const css = gulp.src(wiredep().css);

	return target
		.pipe(inject(
			js.pipe(concat('bower.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest('./js/bower_components/'))
		))
		.pipe(inject(
			css.pipe(concat('bower.min.css'))
			.pipe(cssnano({
				discardComments: {
					removeAll: true
				}
			}))
			.pipe(gulp.dest('./css/bower_components/'))
		))
		.pipe(gulp.dest('./'));

	//TODO - automatically add enqueue to wordpress

	/*
	    <!-- inject:css -->
	    <!-- endinject -->

	    <!-- inject:js -->
	    <!-- endinject -->
	*/
});

// Deleting any file inside the /src folder
gulp.task('clean-source', function () {
	return del(['src/**/*', ]);
});

// Run:
// gulp copy-assets.
// Copy all needed dependency assets files from bower_component assets to themes /js, /scss and /fonts folder. Run this task after bower install or bower update

////////////////// All Bootstrap SASS  Assets /////////////////////////
gulp.task('copy-assets', ['clean-source'], function () {

	////////////////// All Bootstrap 4 Assets /////////////////////////
	// Copy all Bootstrap JS files
	const stream = gulp.src(basePaths.node + 'bootstrap/dist/js/**/*.js')
		.pipe(gulp.dest(basePaths.dev + '/js/bootstrap4'));


	// Copy all Bootstrap SCSS files
	gulp.src(basePaths.node + 'bootstrap/scss/**/*.scss')
		.pipe(gulp.dest(basePaths.dev + '/sass/bootstrap4'));

	////////////////// End Bootstrap 4 Assets /////////////////////////

	// Copy all Font Awesome Fonts
	gulp.src(basePaths.node + 'font-awesome/fonts/**/*.{ttf,woff,woff2,eof,svg}')
		.pipe(gulp.dest('./fonts'));

	// Copy all Font Awesome SCSS files
	gulp.src(basePaths.node + 'font-awesome/scss/*.scss')
		.pipe(gulp.dest(basePaths.dev + '/sass/fontawesome'));

	// Copy jQuery
	gulp.src(basePaths.node + 'jquery/dist/*.js')
		.pipe(gulp.dest(basePaths.dev + '/js'));

	// _s SCSS files
	gulp.src(basePaths.node + 'undescores-for-npm/sass/**/*.scss')
		.pipe(gulp.dest(basePaths.dev + '/sass/underscores'));

	// _s JS files
	gulp.src(basePaths.node + 'undescores-for-npm/js/*.js')
		.pipe(gulp.dest(basePaths.dev + '/js'));

	// Copy Tether JS files
	gulp.src(basePaths.node + 'tether/dist/js/*.js')
		.pipe(gulp.dest(basePaths.dev + '/js'));

	// Copy Tether CSS files
	gulp.src(basePaths.node + 'tether/dist/css/*.css')
		.pipe(gulp.dest(basePaths.dev + '/css'));
	return stream;
});


// Run
// gulp live
// Copies the files to the /dist folder for distributon as simple theme
gulp.task('dist', ['clean-dist', 'scripts', 'styles'], function () {
	gulp.src(['**/*', '!bower_components', '!bower_components/**', '!node_modules', '!node_modules/**', '!src', '!src/**', '!dist', '!dist/**', '!dist-dev', '!dist-dev/**', '!sass', '!sass/**', '!readme.txt', '!readme.md', '!package.json', '!gulpfile.js', '!CHANGELOG.md', '!.travis.yml', '!jshintignore', '!codesniffer.ruleset.xml', '*'])
		.pipe(gulp.dest('dist/'))
});

// Deleting any file inside the /src folder
gulp.task('clean-dist', function () {
	return del(['dist/**/*', ]);
});

// gulp.task('deploy', function () {
//
// 	//Docs - https://github.com/morris/vinyl-ftp
//
// 	const conn = ftp.create({
// 		host: '185.32.188.79',
// 		user: 'legendary@almashopping.pt',
// 		password: 'aD_1X!lWql7E',
// 		parallel: 10
// 	});
//
// 	const globs = [
//         'dist/**'
//     ];
//
// 	const themepath = '/public_html/wp-content/themes/alma';
//
// 	// using base = '.' will transfer everything to /public_html correctly
// 	// turn off buffering in gulp.src for best performance
//
// 	return gulp.src(globs, {
// 			base: 'dist/',
// 			buffer: false
// 		})
// 		.pipe(conn.dest(themepath));
//
// });

// gulp.task('live', function (callback) {
// 	gulpSequence('dist', 'deploy')(callback)
// });

// Run
// gulp dev
// Copies the files to the /dist-dev folder for distributon as theme with all assets and deploy
gulp.task('dist-dev', ['clean-dist-dev', 'scripts', 'styles'], function () {
	gulp.src(['**/*', '!bower_components', '!bower_components/**', '!node_modules', '!node_modules/**', '!dist', '!dist/**', '!dist-dev', '!dist-dev/**', '*'])
		.pipe(gulp.dest('dist-dev/'))
});

// Deleting any file inside the /src folder
gulp.task('clean-dist-dev', function () {
	return del(['dist-dev/**/*', ]);
});

gulp.task('deploy-dev', function () {

	//Docs - https://github.com/morris/vinyl-ftp

	const conn = ftp.create({
		host: 'ftp.dev.legendary.pt',
		user: 'dev@dev.legendary.pt',
		password: 'oLh@Q6Da?fvw',
		parallel: 10,
		reload: true
	});

	const globs = [
        'dist-dev/**'
    ];

	const themepath = '/public_html/forumaveiro/wp-content/themes/forum';

	// using base = '.' will transfer everything to /public_html correctly
	// turn off buffering in gulp.src for best performance


		return gulp.src(globs, {
			base: 'dist-dev/',
			buffer: false
		})
		.pipe(conn.newerOrDifferentSize(themepath))
		.pipe(conn.dest(themepath));

});

gulp.task('dev', function (callback) {
	gulpSequence('dist-dev', 'deploy-dev')(callback)
});
