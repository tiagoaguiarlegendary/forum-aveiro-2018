<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package forum
 */

get_header();

$container   = get_theme_mod( 'forum_container_type' );

?>

<div class="wrapper full-wrapper-xs" id="404-wrapper">

	<div class="<?php echo esc_html( $container ); ?> fullHeight" id="content" tabindex="-1">

		<div class="row fullHeight">

			<div class="col-12 content-area fullHeight" id="primary">

				<main class="site-main fullHeight" id="main">

					<section class="error-404 not-found fullHeight">

						<div class="the-content text-center">

							<h1>ERRO 404</h1>

							<p>A página que esta a tentar aceder não se encontra disponível.</p>

						</div><!-- .page-content -->

					</section><!-- .error-404 -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		if(window.innerHeight > 500)
			fullContainerPage('.full-wrapper-xs');
		else
			$('.full-wrapper-xs').height('60vh');
	});
</script>
