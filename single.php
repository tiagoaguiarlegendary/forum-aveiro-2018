<?php
/**
 * The template for displaying a reasons and suggestions detail.
 *
 * @package forum
 */

get_header();
$container   = get_theme_mod( 'forum_container_type' );

$post_type = get_post_type();

if ( $post_type ) :

	$post_type_data = get_post_type_object( $post_type );
	$post_type_slug = $post_type_data->rewrite['slug'];
	$post_type_name = $post_type_data->name;

	//fields
	$text = get_field($post_type_name.'_text');
	$date = get_field($post_type_name.'_date');
	$gallery = get_field($post_type_name.'_gallery');

else :
	wp_redirect( home_url() ); exit;
endif;

?>

<script>
	if ('<?php echo $post_type;?>' === 'stores') {window.location.replace('http://forumshopping.pt') }
</script>

<div class="wrapper single-wrapper">

	<div class="<?php echo esc_html( $container ); ?> fullHeight">

		<div class="row fullHeight pos-rel">

			<div class="col-12 col-md-4 ml-auto">

				<?php if ($gallery) : ?>

					<div class="col-12 single-<?php echo $post_type_name; ?>-gallery">

						<div class="owl-carousel owl-theme">
								<?php foreach ($gallery as $image) : ?>
										<div class="item">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</div>
								<?php endforeach;?>
						</div>

					</div>

				<?php endif;?>

			</div>

			<div class="col-12 text-container col-md-4 mr-auto custom-scroll">

				<div class="row">

					<div class="col-12">
						<?php if ($date):?>
							<p class="article-date">
								<?php echo $date;?>
							</p>
						<?php endif;?>
					</div>

					<div class="col-12">
						<h1 class="article-title"><?php the_title();?></h1>
					</div>

					<div class="col-12 body-text-14">
						<?php echo $text;?>
					</div>

					<div class="next_post">
						<?php previous_post_link( '%link', "Próximo"); ?>
					</div>

				</div>

			</div>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$('.owl-carousel').owlCarousel({
		margin:30,
		stagePadding: 0,
		items:1,
		nav: true,
		dots: false,
		navText : ['','']
	});

	$(function (){

		if (!isMobile || ( window.innerWidth > 768 && window.innerWidth <= 1024 ))
			fullContainerPage('.single-wrapper');
	});

</script>
