﻿# Installation

## Classic install
- cd to wordpress themes folder
- git clone https://legendNunoC@bitbucket.org/LegendaryPeople/alma.git
- Make sure you have installed Node.js
- Run: `$ npm install`

## WordPress.org install
- Open your WordPress backend
- Click on "Appearance -> Themes"
- Activate the alma theme

# Page Templates

## Blank Template

The `blank.php` template is useful when working with various page builders and can be used as a starting blank canvas.

## Empty Template

The `empty.php` template displays a header and a footer only. A good starting point for landing pages.

## Full Width Template

The `fullwidthpage.php` template has full width layout without a sidebar.
