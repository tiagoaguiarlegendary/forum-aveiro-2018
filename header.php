<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package forum
 */

$container = get_theme_mod('forum_container_type');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="google-site-verification" content="19q8GgQ9Mru33ab3w7oPHwK8_3XkIty5Jy9_kj-IZx4" />
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/src/css/magnific-popup.css"> -->
	<?php wp_head(); ?>

	<?php
	//GLOBAL SCOPE
	$GLOBALS['dir_uri'] = get_template_directory_uri();
	$GLOBALS['page_template'] = basename( get_page_template() );
	$GLOBALS['pages_bg_split'] = array("blank-scroll-container.php","blank-form.php");
	$GLOBALS['pages_bg_white'] = array("services.php");
	$GLOBALS['pages_bg_transparent'] = array("blank-full-bg.php", "tourist-info.php", "brand-kit.php");
	?>

	<script>
		window.cookie = function () {

			var setCookie = function (cname, cvalue, exdays) {
				var d = new Date();
				d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
				var expires = "expires=" + d.toUTCString();

				document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
			}

			var getCookie = function (cname) {
				var name = cname + "=";
				var decodedCookie = decodeURIComponent(document.cookie);
				var ca = decodedCookie.split(';');
				for (var i = 0, l = ca.length; i < l; i++) {
					var c = ca[i];
					while (c.charAt(0) === ' ') {
						c = c.substring(1);
					}
					if (c.indexOf(name) === 0) {
						return c.substring(name.length, c.length);
					}
				}
				return "";
			}

			var checkCookie = function (cname) {
				var cvalue = getCookie(cname);

				return cvalue === "" || typeof cvalue === 'undefined' ? false : cvalue;
			}

			var devareCookie = function (cname) {
				setCookie(cname, "", -1);

				return checkCookie(cname) ? false : 'done';
			}


			return {
				set: setCookie,
				get: getCookie,
				check: checkCookie,
				devare: devareCookie
			}
		}();
	</script>

	<!-- Google Analytics -->
	<script>
	if(cookie.check('cookieConsent') == 'yes' || !cookie.check('cookieConsent')){

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-108263466-1', 'auto');
		ga('send', 'pageview');

	}
	</script>
	<!-- End Google Analytics -->

	<!--[if IE]>
    <link href="<?php echo $GLOBALS['dir_uri'];?>/css/bootstrap-ie9.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/g/html5shiv@3.7.3"></script>
		<script>
			alert('Para ter uma melhor experiência atualize o seu navegador para a versão atual.')
		</script>
  <![endif]-->
  <!--[if lt IE 9]>
  <link href="<?php echo $GLOBALS['dir_uri'];?>/css/bootstrap-ie8.min.css" rel="stylesheet">
  <![endif]-->
</head>

<body <?php body_class(); ?> <?php echo $GLOBALS['page_template'] == 'blank-full-bg.php' ? 'style="background-image : url( ' . get_field("blank_full_bg_bg")['url'] . ' );"' : '' ?>>

<div class="hfeed site" id="page">

	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var $ = jQuery;
	</script>

	<!-- ******************* The Navbar Area ******************* -->
	<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e('Skip to content', 'forum'); ?></a>

		<nav class="navbar navbar-toggleable-md fixed-top navbar-black-links">

		<?php if ('container' == $container) : ?>
			<div class="container mx-md-auto">
		<?php endif; ?>

			<div class="col-md-2 navbar-brand-container">
				<a class="navbar-brand align-self-start" rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
					<?php echo file_get_contents(get_template_directory_uri() . "/svg/src/logoforum.svg"); ?>
				</a>
			</div>

				<span class="toggle-menu-anchor align-self-start">Quero ver </span>

				<div class="navbar-header-menu align-self-start">

					<!-- The WordPress Menu goes here -->
					<?php
				        wp_nav_menu(
				            array(
				                'theme_location'  => 'header-menu',
				                'container_class' => 'collapse navbar-collapse',
				                'container_id'    => 'navbarNavDropdown',
				                'menu_class'      => 'navbar-nav',
				                'fallback_cb'     => '',
				                'menu_id'         => 'main-menu',
				                'walker'          => new WP_Bootstrap_Navwalker(),
				            )
				        );
        	?>
				</div>

				<div class="social-links ml-auto align-self-start hidden-md-down">
					<?php dynamic_sidebar( 'footerlinks' ); ?>
					<!-- <a rel="home" href="<?php //echo esc_url(home_url('/')); ?>" title="<?php //echo esc_attr(get_bloginfo('name', 'display')); ?>"><?php //echo file_get_contents(get_template_directory_uri() . "/svg/brand_logo.svg");?></a> -->
				</div>

				<div class="brand-logo ml-auto align-self-start hidden-md-up">
					<a rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><?php echo file_get_contents(get_template_directory_uri() . "/svg/src/logoforum.svg");?></a>
				</div>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>


		<?php if ('container' == $container) : ?>
			</div><!-- .container -->
		<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->
