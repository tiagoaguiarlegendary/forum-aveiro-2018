<?php
/**
 * Check and setup theme's default settings
 *
 * @package forum
 *
 */
function setup_theme_default_settings() {

	// check if settings are set, if not set defaults.
	// Caution: DO NOT check existence using === always check with == .
	// Latest blog posts style.
	$forum_posts_index_style = get_theme_mod( 'forum_posts_index_style' );
	if ( '' == $forum_posts_index_style ) {
		set_theme_mod( 'forum_posts_index_style', 'default' );
	}

	// Sidebar position.
	$forum_sidebar_position = get_theme_mod( 'forum_sidebar_position' );
	if ( '' == $forum_sidebar_position ) {
		set_theme_mod( 'forum_sidebar_position', 'right' );
	}

	// Container width.
	$forum_container_type = get_theme_mod( 'forum_container_type' );
	if ( '' == $forum_container_type ) {
		set_theme_mod( 'forum_container_type', 'container' );
	}
}
