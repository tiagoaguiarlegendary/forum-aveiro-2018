<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package forum
 */

if ( ! function_exists( 'forum_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function forum_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}
add_filter( 'body_class', 'forum_body_classes' );

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'adjust_body_class' );

if ( ! function_exists( 'adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'change_logo_class' );

if ( ! function_exists( 'change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"' , $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */
if ( ! function_exists( 'forum_post_nav' ) ) :

	function forum_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
				<nav class="container navigation post-navigation">
					<h2 class="sr-only"><?php _e( 'Post navigation', 'forum' ); ?></h2>
					<div class="row nav-links justify-content-between">
						<?php

							if ( get_previous_post_link() ) {
								previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'forum' ) );
							}
							if ( get_next_post_link() ) {
								next_post_link( '<span class="nav-next">%link</span>',     _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'forum' ) );
							}
						?>
					</div><!-- .nav-links -->
				</nav><!-- .navigation -->

		<?php
	}
endif;

//Mark parent navigation active when on custom post type single page

function add_current_nav_class($classes, $item) {

	// Getting the current post details
	global $post;

	// Getting the post type of the current post
	$current_post_type = get_post_type_object(get_post_type($post->ID));
	$current_post_type_slug = $current_post_type->rewrite[slug];

	// Getting the URL of the menu item
	$menu_slug = strtolower(trim($item->url));

	// If the menu item URL contains the current post types slug add the current-menu-item class
	if (strpos($menu_slug,$current_post_type_slug) !== false) {

	   $classes[] = 'active';

	}

	// Return the corrected set of classes to be added to the menu item
	return $classes;

}

add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );


/**
 * Load a component into a template while supplying data.
 *
 * @param string folder - The foder name for the generic template.
 * @param string slug - The slug name for the generic template.
 * @param array params - An associated array of data that will be extracted into the templates scope
 * @param bool output - Whether to output component or return as string.
 * @return string
 */

function args_get_template_part($folder = 'loop-templates', $slug, array $params = array(), $output = true) {
    if(!$output) ob_start();

		if (!$template_file = locate_template("{$folder}/{$slug}.php", false, false)) {
      trigger_error(sprintf(__('Error locating %s for inclusion', 'forum'), $file), E_USER_ERROR);
    }

    extract($params, EXTR_SKIP);
    require($template_file);
    if(!$output) return ob_get_clean();
}

/**
 * in_array function to multidimensional arrays - https://stackoverflow.com/questions/4128323/in-array-and-multidimensional-array
 *
 * @param string needle - The string to look for
 * @param array haystack - The array that will be searched
 * @return bool
 */

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

//Add additional file types to wordpress media library
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	$mimes['eps'] = 'application/postscript';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//Custom login
function loginlogo_img() {
	echo '<style type="text/css">h1 a {background-image: url('.get_bloginfo('template_directory').'/img/cbre-login-logo.png) !important;width:300px!important;height:60px!important;background-size: 75%!important;}</style>';
}
add_action('login_head', 'loginlogo_img');

function loginlogo_url(){
	return "http://www.cbre.pt/";
}
add_action('login_headerurl', 'loginlogo_url');

function loginlogo_title(){
	return "CBRE";
}
add_action('login_headertitle', 'loginlogo_title');
