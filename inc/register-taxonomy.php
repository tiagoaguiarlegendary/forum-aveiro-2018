<?php

add_action( 'init', 'create_taxonomies', 0 );

function create_taxonomies() {

	$labels = array(
		'name'              => _x( 'Gêneros', 'taxonomy general name', 'forum' ),
		'singular_name'     => _x( 'Gênero', 'taxonomy singular name', 'forum' ),
		'search_items'      => __( 'Pesquisa de gêneros', 'forum' ),
		'all_items'         => __( 'Todos os gêneros', 'forum' ),
		'parent_item'       => __( 'Gênero pai', 'forum' ),
		'parent_item_colon' => __( 'Gênero pai:', 'forum' ),
		'edit_item'         => __( 'Editar gênero', 'forum' ),
		'update_item'       => __( 'Atualizar gênero', 'forum' ),
		'add_new_item'      => __( 'Adicionar novo gênero', 'forum' ),
		'new_item_name'     => __( 'Novo nome gênero', 'forum' ),
		'menu_name'         => __( 'Gênero', 'forum' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'genre' ),
	);

	register_taxonomy( 'genre', 'stores', $args );

	$labels = array(
		'name'              => _x( 'Tipos de destaque', 'taxonomy general name', 'forum' ),
		'singular_name'     => _x( 'Tipo de destaque', 'taxonomy singular name', 'forum' ),
		'search_items'      => __( 'Pesquisa de tipos de destaque', 'forum' ),
		'all_items'         => __( 'Todos os tipos de destaque', 'forum' ),
		'parent_item'       => __( 'Tipo de destaque pai', 'forum' ),
		'parent_item_colon' => __( 'Tipo de destaque pai:', 'forum' ),
		'edit_item'         => __( 'Editar tipo de destaque', 'forum' ),
		'update_item'       => __( 'Atualizar tipo de destaque', 'forum' ),
		'add_new_item'      => __( 'Adicionar novo tipo de destaque', 'forum' ),
		'new_item_name'     => __( 'Novo nome de tipo de destaque', 'forum' ),
		'menu_name'         => __( 'Tipo de destaque', 'forum' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'highligth_type' ),
	);

	register_taxonomy( 'highligth_type', 'highligth', $args );

	$labels = array(
		'name'              => _x( 'Tipos de Razões', 'taxonomy general name', 'forum' ),
		'singular_name'     => _x( 'Tipo de Razões', 'taxonomy singular name', 'forum' ),
		'search_items'      => __( 'Pesquisa de tipos de Razões', 'forum' ),
		'all_items'         => __( 'Todos os tipos de Razões', 'forum' ),
		'parent_item'       => __( 'Tipo de Razões pai', 'forum' ),
		'parent_item_colon' => __( 'Tipo de Razões pai:', 'forum' ),
		'edit_item'         => __( 'Editar tipo de Razões', 'forum' ),
		'update_item'       => __( 'Atualizar tipo de Razões', 'forum' ),
		'add_new_item'      => __( 'Adicionar novo tipo de Razões', 'forum' ),
		'new_item_name'     => __( 'Novo nome de tipo de Razões', 'forum' ),
		'menu_name'         => __( 'Tipo de Razões', 'forum' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'reasons_type' ),
	);

	register_taxonomy( 'reasons_type', 'reasons', $args );
}

?>
