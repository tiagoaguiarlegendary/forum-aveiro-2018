<?php

function post_type(){

	//Highlights
	$labels = array(

		'name'                => _x('Destaques', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Destaque', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Destaques', 'forum'),
		'parent_item_colon'   => __('Parent Destaques', 'forum'),
		'all_items'           => __('Todos os Destaques', 'forum'),
		'view_item'           => __('Ver Destaque', 'forum'),
		'add_new_item'        => __('Adicionar Destaque', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar Destaque', 'forum'),
		'update_item'         => __('Atualizar Destaque', 'forum'),
		'search_items'        => __('Procurar Destaque', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('highligth', 'forum'),
		'description'         => __('<h2 class="body-text-15 __lower-lh text-white text-uppercase">o que está a <br><span class="body-text-20 __lower-lh text-white text-fw-black">acontecer</span></h2>', 'forum'),
		'supports'            => array('title' , 'revisions'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'o-que-esta-a-acontecer'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('highligth', $args);


	//Stores
	$labels = array(

		'name'                => _x('Lojas', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Loja', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Lojas', 'forum'),
		'parent_item_colon'   => __('Parent Lojas', 'forum'),
		'all_items'           => __('Todos os Lojas', 'forum'),
		'view_item'           => __('Ver Loja', 'forum'),
		'add_new_item'        => __('Adicionar Loja', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar Loja', 'forum'),
		'update_item'         => __('Atualizar Loja', 'forum'),
		'search_items'        => __('Procurar Loja', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('stores', 'forum'),
		'description'         => __('Lojas do forum Shopping', 'forum'),
		'supports'            => array('title' , 'revisions'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'lojas'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('stores', $args);

	//Jobs
	$labels = array(

		'name'                => _x('Empregos', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Emprego', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Empregos', 'forum'),
		'parent_item_colon'   => __('Parent empregos', 'forum'),
		'all_items'           => __('Todos os empregos', 'forum'),
		'view_item'           => __('Ver Emprego', 'forum'),
		'add_new_item'        => __('Adicionar emprego', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar emprego', 'forum'),
		'update_item'         => __('Atualizar emprego', 'forum'),
		'search_items'        => __('Procurar emprego', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('jobs', 'forum'),
		'description'         => __('Empregos do forum Shopping', 'forum'),
		'supports'            => array('title' , 'revisions'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'empregos'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('jobs', $args);

	//Stores
	$labels = array(

		'name'                => _x('Serviços', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Serviço', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Serviços', 'forum'),
		'parent_item_colon'   => __('Parent Serviços', 'forum'),
		'all_items'           => __('Todos os Serviços', 'forum'),
		'view_item'           => __('Ver Serviço', 'forum'),
		'add_new_item'        => __('Adicionar Serviço', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar Serviço', 'forum'),
		'update_item'         => __('Atualizar Serviço', 'forum'),
		'search_items'        => __('Procurar Serviço', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('services', 'forum'),
		'description'         => __('Serviços do forum Shopping', 'forum'),
		'supports'            => array('title' , 'revisions'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'serviços'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('services', $args);

	//trendy-spot
	$labels = array(

		'name'                => _x('Sugestão', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Sugestões', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Sugestões', 'forum'),
		'parent_item_colon'   => __('Parent sugestões', 'forum'),
		'all_items'           => __('Todas as sugestões', 'forum'),
		'view_item'           => __('Ver sugestão', 'forum'),
		'add_new_item'        => __('Adicionar sugestão', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar sugestão', 'forum'),
		'update_item'         => __('Atualizar sugestão', 'forum'),
		'search_items'        => __('Procurar sugestão', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('suggestions', 'forum'),
		'description'         => __('<h2 class="body-text-15 __lower-lh text-white text-uppercase">sugestões para <br><span class="body-text-20 __lower-lh text-white text-fw-black">família</span></h2>', 'forum'),
		'supports'            => array('title' , 'revisions'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'sugestoes-para-familia'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('suggestions', $args);

	//razões
	$labels = array(

		'name'                => _x('Razões', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Razões', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Razões', 'forum'),
		'parent_item_colon'   => __('Parent Razões', 'forum'),
		'all_items'           => __('Todas as Razões', 'forum'),
		'view_item'           => __('Ver as razões', 'forum'),
		'add_new_item'        => __('Adicionar as razões', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar as razões', 'forum'),
		'update_item'         => __('Atualizar as razões', 'forum'),
		'search_items'        => __('Procurar as razões', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('reasons', 'forum'),
		'description'         => __('As Razões do forum Shopping', 'forum'),
		'supports'            => array('title' , 'revisions'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'razoes'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('reasons', $args);


	//Stories
	$labels = array(

		'name'                => _x('Artigo', 'Post Type General Name', 'forum'),
		'singular_name'       => _x('Artigo', 'Post Type Singular Name', 'forum'),
		'menu_name'           => _x('Insta Stories', 'forum'),
		'parent_item_colon'   => __('Parent Artigo', 'forum'),
		'all_items'           => __('Todas os Artigos', 'forum'),
		'view_item'           => __('Ver os Artigos', 'forum'),
		'add_new_item'        => __('Adicionar Artigos', 'forum'),
		'add_new'             => _x('Adicionar', 'forum'),
		'edit_item'           => __('Editar Artigo', 'forum'),
		'update_item'         => __('Atualizar Artigo', 'forum'),
		'search_items'        => __('Procura Artigo', 'forum'),
		'not_found'           => __('Não encontrado', 'forum'),
		'not_found_in_trash'  => __('Não encontrado no Lixo', 'forum')

	);

	$args = array(

		'labels'              => $labels,
		'label'               => __('artigos', 'forum'),
		'menu_icon'  					 => 'dashicons-format-quote',
		'description'         => __('Os Artigos do forum Shopping', 'forum'),
		'supports'            => array('title' , 'artigos'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite' 			  		=> array('slug' => 'artigos'),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'

	);

	register_post_type('Artigos', $args);

}


add_action('init', 'post_type', 0);


flush_rewrite_rules(); // command to rewrite slug of post_type

?>
