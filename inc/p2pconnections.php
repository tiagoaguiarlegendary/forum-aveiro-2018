<?php

//Example

function connection_destaques_homepage() {
    p2p_register_connection_type( array(
        'name' => 'destaques-home',
        'from' => 'page',
        'to' => 'highligth',
        'title' => 'Destaques do Início'
    ) );
}

add_action( 'p2p_init', 'connection_destaques_homepage' );

?>