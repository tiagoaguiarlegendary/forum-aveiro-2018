<?php

function loadmore(){

	$post_type = $_POST['post_type'];
	$posts_per_page = $_POST['posts_per_page'];
	$order = $_POST['order'];
	$orderby = $_POST['orderby'];
	$content_template = $_POST['content_template'];
  $paged = $_POST['page'];
	$tax_query = json_decode(stripslashes($_POST['tax_query']));
	$meta_query_args = json_decode(stripslashes($_POST['meta_query_args']),true);

	if ($tax_query == false):

		$args = array(
	      'post_status'       => 'publish',
	      'post_type'         => $post_type,
				'order'							=> $order,
				'orderby'						=> $orderby,
	      'posts_per_page'    => $posts_per_page,
	      'paged'             => $paged,
				'meta_query'				=> $meta_query_args
	  );

	else:

		//$terms = json_decode(stripslashes($tax_query->terms)); stripslashes for multiples reasons_type

		$args = array(
	      'post_status'       => 'publish',
				'post_type'					=> $post_type,
				'order'							=> $order,
				'orderby'						=> $orderby,
	      'posts_per_page'    => $posts_per_page,
	      'paged'             => $paged,
				'tax_query' 				=> array(
			      array(
			        'taxonomy' => $tax_query->taxonomy,
			        'field' => $tax_query->field,
			        'terms' => $tax_query->terms
			      )
				)
	  );

		if (sizeof($tax_query->terms) < 1){ array_pop($args); } //remove 'tax_query' arguments

	endif;

	echo args_get_template_part('loop-templates',$content_template,$args);

	exit();

}

add_action('wp_ajax_loadmore', 'loadmore');
add_action('wp_ajax_nopriv_loadmore', 'loadmore');

function filter_store_genre(){

	$posts_per_page = $_POST['posts_per_page'];
	$post_type = $_POST['post_type'];
  $paged = $_POST['page'];
	$term = $_POST['term'];

  $args = array(
      'post_status'       => 'publish',
			'post_type'					=> $post_type,
			'orderby'						=> 'title',
			'order'							=> 'ASC',
      'posts_per_page'    => $posts_per_page,
      'paged'             => $paged,
			'tax_query' 				=> array(
		      array(
		        'taxonomy' => 'genre',
		        'field' => 'slug',
		        'terms' => $term
		      )
			)
  );

	if ($genre_type == 'all'){ array_pop($args); } //remove 'tax_query' arguments

	ob_start();

	echo args_get_template_part('loop-templates','content-stores',$args);

	$the_query = new WP_Query( $args );

	$output = array(
		'content' => ob_get_contents(),
		'max_num_pages' => $the_query->max_num_pages
	);

	ob_end_clean();

	echo json_encode($output);

	//echo json_encode($args);

	wp_reset_postdata();

	exit();

}

add_action('wp_ajax_filter_store_genre', 'filter_store_genre');
add_action('wp_ajax_nopriv_filter_store_genre', 'filter_store_genre');

function filter_reasons_type(){

	$posts_per_page = $_POST['posts_per_page'];
	$post_type = $_POST['post_type'];
  $paged = $_POST['page'];
	$term = json_decode(stripslashes($_POST['term']));

  $args = array(
      'post_status'       => 'publish',
			'post_type'					=> $post_type,
      'posts_per_page'    => $posts_per_page,
      'paged'             => $paged,
			'tax_query' 				=> array(
		      array(
		        'taxonomy' => 'reasons_type',
		        'field' => 'term_id',
		        'terms' => $term
		      )
			)
  );

	if (sizeof($term) < 1){ array_pop($args); } //remove 'tax_query' arguments

	ob_start();

	echo args_get_template_part('loop-templates','content-reason',$args);

	$the_query = new WP_Query( $args );

	$output = array(
		'content' => ob_get_contents(),
		'max_num_pages' => $the_query->max_num_pages
	);

	ob_end_clean();

	echo json_encode($output);

	wp_reset_postdata();

	exit();

}

add_action('wp_ajax_filter_reasons_type', 'filter_reasons_type');
add_action('wp_ajax_nopriv_filter_reasons_type', 'filter_reasons_type');


function liveSearch(){

	$s = $_POST['s'];
	$post_type = $_POST['post_type'];

	$args = array(

		'post_status'       => 'publish',
		'post_type'         => $post_type,
		'posts_per_page' 		=> -1,
		'orderby'						=> 'title',
		'order'							=> 'ASC',
		's' 								=> $s

	);


	$output = array();

	$the_query = new WP_Query($args);

	if ($the_query->have_posts()) :

		while ($the_query->have_posts()) : $the_query->the_post();

			$post_title = get_the_title();
			$store_id = get_field('store_id');

			if (!in_array_r($post_title,$output)) :

				array_push($output, array('title' => $post_title, 'store_id' => $store_id ));

			endif;

		endwhile;

	endif;

	echo json_encode($output);

	wp_reset_postdata();

	exit();

}

add_action('wp_ajax_liveSearch', 'liveSearch');
add_action('wp_ajax_nopriv_liveSearch', 'liveSearch');

function searchStore(){

	$s = $_POST['s'];
	$key = $_POST['key'];

	$args = array(

		'post_status'       => 'publish',
		'post_type'         => 'stores',
		'posts_per_page' 		=> 1,
		'orderby'						=> 'title',
		'order'							=> 'ASC'

	);

	if ($key == 'title') :

		$args['s'] = $s;

	else :

		$args['meta_key'] = $meta_key;
		$args['meta_value'] = $s;

	endif;

	ob_start();

	$the_query = new WP_Query($args);

	if ($the_query->have_posts()) :

		while ($the_query->have_posts()) : $the_query->the_post();

			get_template_part( 'loop-templates/content', 'store-info' );

		endwhile;

	endif;

	$output = ob_get_contents();

	ob_end_clean();

	echo $output;

	wp_reset_postdata();

	exit();

}

add_action('wp_ajax_searchStore', 'searchStore');
add_action('wp_ajax_nopriv_searchStore', 'searchStore');

?>
