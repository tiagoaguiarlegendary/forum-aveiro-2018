<?php
header('Content-type: text/plain; charset=utf-8');
require_once('../../../../wp-blog-header.php');
define('WP_USE_THEMES', false);


/*********
 Insta Data 
*********/

		  $posts = get_posts(array(

		        'post_status'       => 'publish',
		        'post_type'         => 'artigos',
		        'posts_per_page'    => -1,
		        'order'             => 'DESC',
		    ));


 $insta = array();

foreach($posts as $post):

  $instacontent = get_field('stories_content');


	array_push($insta, ['title' => get_the_title(), 'content' => get_field('stories_content')]);

endforeach;
echo wp_json_encode($insta, JSON_UNESCAPED_UNICODE);
?>
