<?php
/**
 * forum modify editor
 *
 * @package forum
 */

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
  add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

// Add TinyMCE style formats.
add_filter( 'mce_buttons_2', 'forum_tiny_mce_style_formats' );

function forum_tiny_mce_style_formats( $styles ) {

    array_unshift( $styles, 'styleselect' );
    return $styles;
}

add_filter( 'tiny_mce_before_init', 'forum_tiny_mce_before_init' );

function forum_tiny_mce_before_init( $settings ) {

  $style_formats = array(
      array(
          'title' => 'Lead Paragraph',
          'selector' => 'p',
          'classes' => 'lead',
          'wrapper' => true
          ),
      array(
          'title' => 'Small',
          'inline' => 'small'
      ),
      array(
          'title' => 'Blockquote',
          'block' => 'blockquote',
          'classes' => 'blockquote',
          'wrapper' => true
      ),
			array(
          'title' => 'Blockquote Footer',
          'block' => 'footer',
          'classes' => 'blockquote-footer',
          'wrapper' => true
      ),
			array(
          'title' => 'Cite',
          'inline' => 'cite'
      )
  );

    if ( isset( $settings['style_formats'] ) ) {
      $orig_style_formats = json_decode($settings['style_formats'],true);
      $style_formats = array_merge($orig_style_formats,$style_formats);
    }

    $settings['style_formats'] = json_encode( $style_formats );
    return $settings;
}

function remove_from_menu() {

  remove_menu_page('edit.php');

  remove_menu_page('edit-comments.php');

}

add_action('admin_menu', 'remove_from_menu');

function hide_editor() {
  // Get the Post ID.

  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

  if( !isset( $post_id ) ) return;

  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = basename( get_page_template() );
  $to_exclude_list = array('homepage.php','list-stores.php','details.php','list.php', 'tourist-info.php',
  'jobs.php', 'services.php', 'blank-form.php', 'blank-scroll-container.php','brand-kit.php'); // the filename of the page template


  if(in_array($template_file , $to_exclude_list)){
    remove_post_type_support('page', 'editor');
  }

}

add_action( 'admin_head', 'hide_editor' );
