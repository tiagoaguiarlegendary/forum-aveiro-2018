<?php
/**
 * The template for displaying a highligth detail.
 *
 * @package forum
 */

get_header();
$container   = get_theme_mod( 'forum_container_type' );

//fields
$dimension = get_field('destaque_img_dimension');
$text = get_field('destaque_text');
$date_start = get_field('destaque_date_start');
$date_end = get_field('destaque_date_end');
$gallery = get_field('destaque_gallery');
$destaque_desc = get_field('destaque_description');
?>

<div class="wrapper single-wrapper<?php echo $dimension == 'wide' ? null : " portrait-wrapper" ?>">

	<div class="<?php echo esc_html( $container ); ?> fullHeight">

		<div class="row fullHeight pos-rel">

			<?php if ($dimension == 'wide') : ?>

			<div class="col-12 col-md-8 mx-auto">
				<div class="row">

			<?php endif;?>

			<div class="col-12<?php echo $dimension == 'wide' ? null : " col-md-4 ml-auto" ?>">

				<?php if ($gallery) : ?>

					<div class="col-12 single-highligth-gallery">

						<div class="owl-carousel owl-theme">
								<?php foreach ($gallery as $image) : ?>
										<div class="item">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</div>
								<?php endforeach;?>
						</div>

					</div>

				<?php endif;?>

			</div>

			<div class="col-12 text-container <?php echo $dimension == 'wide' ? "" : "col-md-4 mr-auto custom-scroll" ?>">

				<div class="row">

					<div class="col-12">
						<h1 class="article-title"><?php the_title();?></h1>
					</div>

					<div class="col-12 body-text-14 body-desc">
						<?php echo $destaque_desc;?>
					</div>

					<div class="col-12 body-text-20">
						<?php echo $text;?>
					</div>

				</div>

			</div>

			<?php if ($dimension == 'wide') : ?>

				</div>
			</div>

			<?php endif;?>
			<div class="previous_highlight">
				<?php previous_post_link( '%link', "Anterior"); ?>
			</div>
			<div class="next_highlight">
				<?php next_post_link('%link', "Próximo") ?>
			</div>
		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$('.owl-carousel').owlCarousel({
		margin:30,
		stagePadding: 0,
		items:1,
		nav: true,
		dots: true,
		navText : ['','']
	});

	$(function (){
		if (!isMobile || ( window.innerWidth > 768 && window.innerWidth <= 1024 ))
			fullContainerPage('.portrait-wrapper');
	});

</script>
