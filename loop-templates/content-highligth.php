<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	$the_query = new WP_Query( $params );

	if ($the_query->have_posts()) :

		$i = 0;

		while ($the_query->have_posts() && $i < 4 ) : $the_query->the_post(); //4 the number of highligth on the homepage

		//fields
		$img = get_field('destaque_img_destaque');
		//$dimension = get_field('destaque_img_dimension');
		$description = get_field('destaque_description');
		$cta = get_field('destaque_cta') == '' ? get_permalink() : get_field('destaque_cta');
		$cta_target = get_field('destaque_cta') == '' ? '_self' : '_blank';
		$date_start = get_field('destaque_date_start');
		// $date_end = get_field('destaque_date_end');
		$highlight_type = get_terms('highligth_type');

		$scheme_type = $highlight_type['slug'] == 'evento' ?  "http://schema.org/Event" : "http://schema.org/Article";
		?>

		<div class="col-12 col-md-3 item-type-<?php echo $i?>">

			<article itemscope itemtype="<?php echo $scheme_type?>" class="item-hover item-highlight" id="post-highlight-<?php the_ID(); ?>">

				<header class="item-highlight-header">

					<a target="<?php echo $cta_target;?>" href="<?php echo $cta;?>"><img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>"></a>

				</header><!-- .entry-header -->

				<div class="item-highlight-content">

					<div class="item-highlight-content-header">
						<p class="article-date text-fw-regular mb-2">
							<?php 
							if ($date_start != '') :
								echo $date_start;
							endif;
							?>
						</p>
					</div>

					<div class="item-highlight-content-body">
						<a target="<?php echo $cta_target;?>" href="<?php echo $cta;?>"><h3 class="article-title text-fw-regular __fs-25"><?php the_title();?></h3></a>
						<div class="item-highligth-content-footer">
							<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg"); ?>
						</div>
					</div>


				</div><!-- .entry-content -->

			</article><!-- #post-## -->

		</div>

		<?php

		$i < 4 ? $i++ : $i = 0;

		endwhile;

	endif;

	wp_reset_postdata();
?>
