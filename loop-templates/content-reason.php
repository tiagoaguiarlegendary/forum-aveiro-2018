<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	$the_query = new WP_Query( $params );

	$teste = '1';

	if ($the_query->have_posts()) :


		while ($the_query->have_posts() ) : $the_query->the_post();

		//fields
		$the_ID = get_the_ID();
		$img = get_field('reasons_img');
		$frame = get_field('reasons_bg_frame');
		$description = get_field('reasons_description');
		$cta = get_field('reasons_cta') == '' ? get_permalink() : get_field('reasons_cta');
		$cta_target = get_field('reasons_cta') == '' ? '_self' : '_blank';
		$date = get_field('reasons_date');
		$reason_type = get_the_terms($the_ID,'reasons_type');
		$reason_type_color = get_field('reason_type_color','term_'.$reason_type[0]->{'term_id'});

		?>

		<div class="grid-masonry-item">
			<!-- start #post-reason-<?php echo $the_ID; ?> -->

			<article itemscope itemtype="http://schema.org/Article" class="item-reason" id="post-reason-<?php echo $the_ID; ?>">

				<header class="item-reason-header <?php echo $frame ? '__frame-'.$frame : '';?>">

					<a target="<?php echo $cta_target;?>" href="<?php echo $cta;?>"><img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>"></a>

				</header><!-- .item-reason-header -->

				<div class="item-reason-content">

					<?php if ($date != '') : ?>

						<div class="item-reason-content-header">
							<p class="article-date __fs-10"><?php echo $date?></p>
						</div>

					<?php endif;?>

					<div class="item-reason-content-body">
						<a target="<?php echo $cta_target;?>" href="<?php echo $cta;?>"><h3 class="article-title __fs-14" style="color:<?php echo $reason_type_color;?>"><?php the_title();?></h3></a>
						<?php if($description) : ?><p class="body-text-15"><?php echo $description; ?></p><?php endif;?>
					</div>

				</div><!-- .item-reason-content -->

			</article>

			<!-- end #post-reason-<?php the_ID(); ?> -->
		</div>
		<?php

		endwhile;

	endif;

	wp_reset_postdata();
?>
