<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	//fields
	$storeid = get_field('store_id');
	$name = get_the_title();
	$dayofweek = date('w', strtotime($date));
	$time_open = $dayofweek < 5 ? get_field('store_time_open') : get_field('store_time_open_weekend');
	$time_close = $dayofweek < 5 ? get_field('store_time_close') : get_field('store_time_close_weekend');
	$piso = get_field('store_piso');
	$number = get_field('store_number');
	$contact = get_field('store_contact');
	$email = get_field('store_email');
	$website = get_field('store_website');
	$genre = get_the_terms(get_the_ID() ,'genre');

	$website_front = str_replace(["http://www.","https://www.","https://","https://"],'',$website);

	if(substr($website_front, -1) == '/') {
    $website_front = substr($website_front, 0, -1);
	}

?>


<article id="<?php echo $storeid?>" itemscope itemtype="http://schema.org/Brand" class="item-store white-info">

	<div class="item-store-container">

		<div class="item-store-info">

			<p class="item-store-info-title"><b><?php echo $name?></b></p>
			<hr class="divider">
			
			<?php if (sizeof($genre > 0)) : ?>
				<p><b>C.</b> <?php echo $genre[0]->{'name'};?></p>
			<?php endif;?>

			<p><b>H. </b><?php echo str_replace(":","h",$time_open);?> - <?php echo str_replace(":","h",$time_close);?></p>
			<p>Piso <?php echo $piso;?> Loja <?php echo $number?></p>

			<?php if( $contact ) : ?>
				 <p><b>T. </b><a target="_blank" href="tel:<?php echo $contact;?>"><?php echo $contact;?></a></p>
			<?php endif;?>
			<?php if( $email ) : ?>
				 <p><a target="_top" href="mailto:<?php echo $email;?>"><?php echo $email;?></a></p>
			<?php endif;?>
			<?php if( $website ) : ?>
				 <p><a target="_blank" href="<?php echo $website;?>"><?php echo $website_front?></a></p>
			<?php endif;?>

		</div>

	</div>

</article>
