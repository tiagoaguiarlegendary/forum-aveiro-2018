<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	$the_query = new WP_Query( $params );
	$instacontent = array();
	$insta = array();
	if ($the_query->have_posts()) : ?>

  <div class="owl-carousel owl-theme">
    <?php
		while ($the_query->have_posts()) : $the_query->the_post();?>

				<?php

				//fields
       	$title = get_field('blog_title');
       	$img = get_field('imagem_destaque');
				?>

          <div class="item">
            <img src="<?php echo $img; ?>" alt=" <?php echo $title; ?>" data-title="<?php echo $title; ?>" data-desc="<?php echo $desc; ?>">

						<script>
							$(document).ready(function(){

								$('[data-title="<?php echo $title; ?>"]').magnificPopup({
												items: [
													<?php for ($i=0;$i<count(get_field('stories_content'));$i++){ ?>
															{
																src: '<?php if(count(get_field('stories_content')[$i]['stories_image']))
																{ echo get_field('stories_content')[$i]['stories_image']; }?>',
																type: '<?php if(count(get_field('stories_image') > 0)) {echo "image";} ?>'
															},
													<?php } ?> //End of php Loop
												],
												gallery: {
													enabled: true
												},
												callbacks: {
												open: function() {
													retrieveInfo();
													timer.startTimer();
												},
												change: function() {
											    timer.timerChange();
											  },
												close: function() {
													console.log("closed");
													timer.stopTimer();
													console.log('timerstate',timer.timerState);
													console.log('this.timer',timer);
												},
												type: 'image'
											} //end of callbacks
										}); //end of magnificPopup
									});

						</script>
          </div>
		<?php

				endwhile;

			endif;

			wp_reset_postdata();
		?>


		</div>
