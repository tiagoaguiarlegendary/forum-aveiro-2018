<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	$the_query = new WP_Query( $params );

	if ($the_query->have_posts()) :

		$i = 0;

		while ($the_query->have_posts()) : $the_query->the_post();

		//fields
		$img = get_field('as_nossas_pessoas_img_destaque');
		$description = get_field('as_nossas_pessoas_description');
		$cta = get_field('as_nossas_pessoas_cta') == '' ? get_permalink() : get_field('as_nossas_pessoas_cta');
		$date_start = get_field('as_nossas_pessoas_date_start');
		$date_end = get_field('as_nossas_pessoas_date_end');
		$hashtags = get_field('as_nossas_pessoas_hashtags');
		?>

		<div class="col-12 col-md-3 item-type-<?php echo $i % 2?>">

			<article itemscope itemtype="http://schema.org/Article" class="item-as-nossas-pessoas" id="post-as-nossas-pessoas-<?php the_ID(); ?>">

				<header class="item-as-nossas-pessoas-header">

					<a href="<?php echo $cta;?>" style="background-image: url('<?php echo $img['url'];?>')"></a>

				</header><!-- .entry-header -->

				<div class="item-as-nossas-pessoas-content">

					<div class="item-as-nossas-pessoas-content-header">
						<p class="article-date primary-color">
							<?php
							if ($date_start != '' && $date_end != '') :
								echo $date_start . ' - ' . $date_end;
							elseif ($date_start != '' && $date_end == '') :
								echo $date_start;
							else :
								echo 'Permanente';
							endif;
							?>
						</p>
					</div>

					<div class="item-as-nossas-pessoas-content-body">
						<a href="<?php echo $cta;?>"><h3 class="article-title __fw-regular"><?php the_title();?></h3></a>
						<p class="body-text-15"><?php echo $description;?></p>
						<p class="body-text-15 text-green"><?php echo $hashtags;?></p>
					</div>

				</div><!-- .entry-content -->

			</article><!-- #post-## -->

		</div>

		<?php

		$i++;

		endwhile;

	endif;

	wp_reset_postdata();
?>
