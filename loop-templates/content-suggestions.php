<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	$the_query = new WP_Query( $params );

	if ($the_query->have_posts()) :

		$i = 0;

		while ($the_query->have_posts() ) : $the_query->the_post(); //4 the number of highligth on the homepage

		//fields
		$img = get_field('suggestions_img_destaque');
		$dimension = get_field('suggestions_img_dimension');
		$description = get_field('suggestions_description');
		$cta = get_field('suggestions_cta') == '' ? get_permalink() : get_field('suggestions_cta');
		$cta_target = get_field('suggestions_cta') == '' ? '_self' : '_blank';
		$date_start = get_field('suggestions_date_start');
		$date_end = get_field('suggestions_date_end');

		?>

		<div class="col-12 col-md-3 item-type-<?php echo $i % 2?>">

			<article itemscope itemtype="http://schema.org/Event" class="item-hover item-suggestions" id="post-suggestions-<?php the_ID(); ?>">

				<header class="item-suggestions-header">

					<a target="<?php echo $cta_target;?>" href="<?php echo $cta;?>"><img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>"></a>

				</header><!-- .entry-header -->

				<div class="item-suggestions-content">

					<div class="item-suggestions-content-header">
						<p class="article-date primary-color text-fw-regular mb-2">
							<?php
							if ($date_start != '' && $date_end != '') :
								echo $date_start . ' - ' . $date_end;
							elseif ($date_start != '' && $date_end == '') :
								echo $date_start;
							else :
								echo '';
							endif;
							?>
						</p>
					</div>

					<div class="item-suggestions-content-body">
						<a target="<?php echo $cta_target;?>" href="<?php echo $cta;?>"><h3 class="article-title text-fw-regular __fs-25"><?php the_title();?></h3></a>
						<?php if($description) : ?><p class="body-text-15"><?php  echo $description; ?></p><?php endif;?>
					</div>

				</div><!-- .entry-content -->

			</article><!-- #post-## -->

		</div>

		<?php

		$i++;

		endwhile;

	endif;

	wp_reset_postdata();
?>
