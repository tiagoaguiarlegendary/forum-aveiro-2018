<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	//fields
	$store = get_field('job_store');
	$img_store = get_field('store_logo', $store->ID);
	$title = get_field('job_title');
	$description = get_field('job_description');
	$mailto = get_field('job_mailto');
	$subject = get_field('job_mailto_subject');

?>

	<article itemscope itemtype="http://schema.org/Brand" class="item-job">
		<a class="d-block text-decoration-none" href="mailto:<?php echo $mailto;?>?Subject=<?php echo $subject?>" target="_top">
			<header>
					<img src="<?php echo $img_store['url'];?>" alt="<?php echo $img_store['alt'];?>">
			</header>
		</a>

		<div class="item-job-content">

			<div>
				<hr class="divider">
			</div>

			<div class="item-job-header">
				<a class="d-block text-decoration-none" href="mailto:<?php echo $mailto;?>?Subject=<?php echo $subject?>" target="_top">
					<h3 class="body-text-20"><b><?php echo $title;?></b></h3>
				</a>
			</div>

			<div class="item-job-body mb-4">
				<p class="body-text-15"><?php echo $description;?></p>
			</div>

			<div class="item-job-footer">
				<a class="text-uppercase primary-color text-decoration-none" href="mailto:<?php echo $mailto;?>?Subject=<?php echo $subject?>" target="_top">Candidata-te</a>
			</div>

		</div>

	</article>
