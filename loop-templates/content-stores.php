<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

$the_query = new WP_Query( $params );

if ($the_query->have_posts()) :

		$i = 0;

		while ($the_query->have_posts()) : $the_query->the_post();

			//fields
			$name = get_the_title();
			$logo = get_field('store_logo');
			$time_open = get_field('store_time_open');
			$time_close = get_field('store_time_close');
			$piso = get_field('store_piso');
			$number = get_field('store_number');
			$contact = get_field('store_contact');
			$email = get_field('store_email');
			$website = get_field('store_website');
			$promo = get_field('store_promotion');
			$promo_link = get_field('store_promotion_link');
			$genre = get_the_terms(get_the_ID() ,'genre');

			$website_front = str_replace(["http://www.","https://www.","https://","https://"],'',$website);

			if(substr($website_front, -1) == '/') {
		    $website_front = substr($website_front, 0, -1);
			}

			if ($logo):?>

			<div class="col-12 col-sm-6 col-md-3 col-xlg-2 mb-90 px-1 <?php if($i % 2 == 0){echo "item-type-1";}else{echo "item-type-0";} ?>" data-i=<?php echo $i;?>>

				<article itemscope itemtype="http://schema.org/Brand" class="item-store" id="store-id-<?php the_ID(); ?>">

					<div class="item-store-logo">
						<img src="<?php echo $logo['url']?>" alt="<?php echo $logo['alt']?>">
					</div>

					<div class="item-store-container">
						<?php if ($promo != ''):?>
						<a class="item-store-info-promo" href="<?php echo $promo_link ? $promo_link : '' ?>"><?php echo $promo;?>%</a>
						<?php endif;?>

						<div class="item-store-info">

							<p class="item-store-info-title"><b><?php echo $name?></b></p>
							<?php if (sizeof($genre > 0)) : ?>
								<p><b>C.</b> <?php echo $genre[0]->{'name'};?></p>
							<?php endif;?>

							<p><b>H. </b><?php echo str_replace(":","h",$time_open);?> - <?php echo str_replace(":","h",$time_close);?></p>
							<p>Piso <?php echo $piso;?> Loja <?php echo $number?></p>

							<?php if( $contact ) : ?>
								 <p><b>T. </b><a target="_blank" href="tel:<?php echo $contact;?>"><?php echo $contact;?></a></p>
							<?php endif;?>
							<?php if( $email ) : ?>
								 <p><a target="_top" href="mailto:<?php echo $email;?>"><?php echo $email;?></a></p>
							<?php endif;?>
							<?php if( $website ) : ?>
								 <p><a target="_blank" href="<?php echo $website;?>"><?php echo $website_front;?></a></p>
							<?php endif;?>

						</div>

						<a class="item-store-info-dismiss">-</a>

					</div>

				</article>

			</div>

<?php
		endif;
		$i++;

	endwhile;

endif;
wp_reset_postdata();

?>
