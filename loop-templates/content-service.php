<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	$the_query = new WP_Query( $params );

	if ($the_query->have_posts()) :

		while ($the_query->have_posts()) : $the_query->the_post();?>

		<div class="col-sm-4">

			<article itemscope itemtype="http://schema.org/Service" class="item-service" id="post-service-<?php the_ID(); ?>">

				<?php

				//fields
				$icon = get_field('service_icon');
				$name = get_the_title();
				$description = get_field('service_description');

				?>

				<header class="item-service-header">

					<?php

						echo '<img src="' . $icon['url'] . '" alt="' . $icon['alt'] . '">';

					?>

					<p><?php echo $name;?></p>

				</header><!-- .entry-header -->


				<div class="item-service-content">

					<div class="item-service-content-body">
						<p class="body-text-15"><?php echo $description;?></p>
					</div>

				</div><!-- .entry-content -->

				<span class="open-btn"></span>

			</article><!-- #post-## -->

		</div>

		<?php

		$i++;

		endwhile;

	endif;

	wp_reset_postdata();
?>
