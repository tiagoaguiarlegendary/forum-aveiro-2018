<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package forum
 */

?>

<?php

	//fields
	$text = get_sub_field('brand_kit_cta_group_text');
	$img = get_sub_field('brand_kit_cta_group_img');

?>


<article itemscope itemtype="http://schema.org/Brand" class="col-12 col-md-6">

	<div class="item-brand-kit-cta-group">

		<div class="row">

			<div class="col-8 item-brand-kit-cta-group-img display-flex-center">
				<img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>">
			</div>

			<div class="col-4 item-brand-kit-cta-group-content pos-rel">
				<hr class="divider">
				<?php if ($text) : ?><p class="body-text-15 mt-5"><?php echo $text;?></p> <?php endif;?>

				<?php
				if( have_rows('brand_kit_cta_group_btn') ):
					while( have_rows('brand_kit_cta_group_btn') ): the_row();

					//fields
					$file = get_sub_field('brand_kit_cta_group_file');
					$label = pathinfo($file['url'],PATHINFO_EXTENSION);?>

						<a class="text-uppercase" download href="<?php echo $file['url'];?>"><?php echo $label?></a>
					<?php endwhile;
				endif;
				?>
			</div>

		</div>

	</div>

</article>
