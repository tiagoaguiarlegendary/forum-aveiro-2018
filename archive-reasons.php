<?php
/**
 *
 * Archive page for highligth and suggestions post type
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper list-wrapper">

	<div class="<?php echo esc_html( $container ); ?> fullHeight">

			<div class="row fullHeight">

				<?php

				$reasons_type = get_terms('reasons_type', array(
					'hide_empty' => true,
					'orderby' => 'name',
				));

				$next_row = '';

				if (sizeof($reasons_type) > 0) :

				$next_row = 'col-md-10';
				?>

				<div class="col-12 col-md-2">
					<div class="row">
						<div class="col-12 filter-container">

							<div class="loading-square"></div>

							<div class="info-box mb-5">
								<p class="body-text-12 text-uppercase text-white text-ls-2-half">escolhe <br>as tuas <br><span class="body-text-20 __lower-lh text-white text-fw-black">razões</span></p>
							</div>

							<div class="filter-reasons">
								<?php foreach ($reasons_type as $reason) :

									$reason_type_color = get_field('reason_type_color','term_'.$reason->{'term_id'});

								?>
										<style>
											.filter-reasons input.reason-type-<?php echo $reason->{'slug'};?>[type='checkbox']+label:before{
												color: #fff;

												content: '';
												border: 1px solid <?php echo $reason_type_color;?>;
												background: transparent no-repeat center center;

											}
											.filter-reasons input.reason-type-<?php echo $reason->{'slug'};?>[type='checkbox']+label:hover:before
											{
												border: 3px solid <?php echo $reason_type_color;?>;
											}
											.filter-reasons input.reason-type-<?php echo $reason->{'slug'};?>[type='checkbox']:checked+label:before
											{
												background: transparent url(data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0A%3Csvg%20version%3D%221.1%22%20id%3D%22Camada_1%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20xmlns%3Axlink%3D%22http%3A//www.w3.org/1999/xlink%22%20x%3D%220px%22%20y%3D%220px%22%0A%09%20viewBox%3D%220%200%2025.7%2025.7%22%20style%3D%22enable-background%3Anew%200%200%2025.7%2025.7%3B%22%20xml%3Aspace%3D%22preserve%22%3E%0A%3Cstyle%20type%3D%22text/css%22%3E%0A%09.st0%7Bfill%3Anone%3Bstroke%3A%23<?php echo substr($reason_type_color,1);?>%3Bstroke-miterlimit%3A10%3B%7D%0A%3C/style%3E%0A%3Cg%3E%0A%09%3Cline%20id%3D%22XMLID_2_%22%20class%3D%22st0%22%20x1%3D%226.4%22%20y1%3D%226.4%22%20x2%3D%2219.4%22%20y2%3D%2219.4%22/%3E%0A%09%3Cline%20id%3D%22XMLID_1_%22%20class%3D%22st0%22%20x1%3D%2219.4%22%20y1%3D%226.4%22%20x2%3D%226.4%22%20y2%3D%2219.4%22/%3E%0A%3C/g%3E%0A%3C/svg%3E%0A) no-repeat center center;
											}
										</style>
										<ul>
											<!-- reason type : <?php echo $reason->{'name'} ?> -->
											<li>
												<input type="checkbox" class="reason-type-<?php echo $reason->{'slug'};?>" name="reasons-type" id="<?php echo $reason->{'slug'};?>" value="<?php echo $reason->{'term_id'};?>">
												<label for="<?php echo $reason->{'slug'};?>"><?php echo $reason->{'description'};?></label>
											</li>
										</ul>
								<?php endforeach ?>
							</div>

						</div>
					</div>
				</div>

				<?php endif;?>

				<div class="col-12 <?php echo $next_row;?> content-area scroll-container">

					<main id="main" class="site-main" role="main" data-append-on=".grid-masonry">

							<div class="row grid-masonry">
								<div class="grid-sizer"></div>
								<?php
									$posts_per_page = 12;
									$post_type = 'reasons';
									$template_content = 'content-reason';

									$args = array(

										'post_status'       => 'publish',
										'post_type'         => $post_type,
										'posts_per_page' 		=> $posts_per_page

									);

									args_get_template_part('loop-templates',$template_content,$args)

								?>
							</div>

					</main><!-- #main -->

					<div id="loadmore-container" class="col-12 content-area text-center">

						<div class="row">

							<div class="col-12">

								<button type="button" onclick="loadmore.get();" class="btn-load-more" name="load-more"></button>

							</div>

						</div>

					</div> <!-- #loadmore-container end -->

				</div>

			</div><!-- .row end -->

		</div>

</div><!-- Wrapper end -->

<?php

	$the_query = new WP_Query( $args );
	$max_num_pages = $the_query->max_num_pages;

	wp_reset_postdata();
?>

<?php get_footer(); ?>

<script>
	var msnry;

	$(function (){
		if (!isMobile){
			$('.wrapper-footer').addClass('float-footer');
			$('html, body').addClass('overflow-hidden-y');
		}

		fullContainerPage('.list-wrapper');

		//item-reason hover animation
		$("#main").on({
		 	mouseenter: function() {
		    $(this).parents('.item-reason').addClass('__on-hover');
		  },
			mouseleave: function() {
		    $(this).parents('.item-reason').removeClass('__on-hover');
		  }
		},'.item-reason a');

		var grid = '.grid-masonry';

		$(grid).imagesLoaded(function () {

			msnry = new Masonry( grid, {
				itemSelector: '.grid-masonry-item',
				columnWidth: '.grid-sizer',
				horizontalOrder: true,
				gutter: 20,
				percentPosition: true
			});

    });

		loadmore.init({
			post_type : '<?php echo $post_type?>',
			posts_per_page : '<?php echo $posts_per_page?>',
			max_num_pages : '<?php echo $max_num_pages ?>',
		}, '<?php echo $template_content?>','#main');

		filter.init({
			post_type : '<?php echo $post_type?>',
			posts_per_page : '<?php echo $posts_per_page?>',
			max_num_pages : '<?php echo $max_num_pages ?>',
			taxonomy : 'reasons_type',
			field : 'term_id',
			action : 'filter_reasons_type'
		}, 'input[name="reasons-type"]');


	});
</script>
