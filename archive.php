<?php
/**
 *
 * Archive page for highligth and suggestions post type
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<?php
$post_type = get_post_type();

if ( $post_type ) :

	$post_type_data = get_post_type_object( $post_type );
	$post_type_slug = $post_type_data->rewrite['slug'];
	$post_type_name = $post_type_data->name;

	$posts_per_page = 4;
	$template_content = 'content-'.$post_type_name;

else :
		//redirect to 404
endif;

?>

<div class="wrapper list-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">


			<div class="col-12 col-md-12 content-area" >

				<?php if ($post_type_slug == 'sugestoes-para-familia') : ?>

				<div class="col-12 col-md-5 title-container">
					<h1 class="article-title text-fw-black"><span class="text-fw-regular">As Nossas</span><br>Sugestões</h1>
				</div>

				<?php endif;?>

				<main id="main" class="site-main" role="main">

					<div class="row">

						<?php

						if ( $post_type ) :

							$args = array(

								'post_status'       => 'publish',
								'post_type'         => $post_type_name,
								'posts_per_page' 		=> $posts_per_page

							);

							args_get_template_part('loop-templates',$template_content,$args);

						endif;

						?>
					</div>

				</main>

				<div id="loadmore-container" class="col-12 content-area text-center">

					<div class="row">

						<div class="col-12">

							<button type="button" onclick="loadmore.get();" class="btn-load-more" name="load-more"></button>

						</div>

					</div>

				</div> <!-- #loadmore-container end -->

			</div>

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->
<?php get_footer(); ?>

<?php

	$the_query = new WP_Query( $args );
	$max_num_pages = $the_query->max_num_pages;

	wp_reset_postdata();
?>


<script>

	fullContainerPage('.list-wrapper',true);

	loadmore.init({
		post_type : '<?php echo $post_type_name?>',
		posts_per_page : '<?php echo $posts_per_page?>',
		max_num_pages : '<?php echo $max_num_pages ?>',
	}, '<?php echo $template_content?>');

</script>
