<?php

/**

 * Template Name: Blog Template

 *

 *

 * @package forum

 */



get_header();

$container = get_theme_mod( 'forum_container_type' );

?>

<div class="time" style="position: fixed; top: 3px; height: 5px; background-color: #ffca00; ">
</div>
<div class="wrapper wrapper-split bg-split fullHeight">



	<div class="<?php echo esc_html( $container ); ?> fullHeight">



		<div class="row fullHeight">



			<div class="col-12 content-area fullHeight">



				<main class="site-main fullHeight" role="main">

						<div class="content fullHeight container">

              <div class="row">

                <div class="col-md-4">
                  <div class="title-bg">
                    <h1>LIFEinstYLE</h1>
										<p class="body-text-14 body-desc">Já segues o nosso <a href="https://www.instagram.com/forumaveiro/" target="_blank">Instagram</a>?<br>
											O melhor para a tua vida está partilhado<br>
											  nas instastories do nosso perfil. <br>
											 <a href="https://www.instagram.com/forumaveiro/" target="_blank">Segue-nos!</a>
										</p>

                  </div>

                  <div class="blog-content">
                    <h2 class="blog-title"></h2>
                    <div class="divider">
  										<?php echo file_get_contents(get_template_directory() . "/svg/yellow-triangle.svg"); ?>
  									</div>
                    <p class="blog-desc"></p>
                  </div>

                </div>

                <div class="col-md-8">
                  <?php
									$args = array(

										'post_status'       => 'publish',
										'post_type'         => 'artigos',
										'orderby'						=> 'title',
										'order'							=> 'DESC',
										'posts_per_page' 		=> 6
									);

									args_get_template_part('loop-templates','content-blog',$args)
									?>
                </div>

              </div>

						</div>

				</main><!-- #main -->



				<div class="line translate-center"></div>



			</div><!-- #primary -->



		</div><!-- .row end -->



	</div><!-- Container end -->



</div><!-- Wrapper end -->


<?php get_footer();?>
<?php	include get_template_directory() . '/inc/blog-scripts.php'; ?>

<script>

$( document ).ready(function() {


setTimeout(function() {
      $(".owl-item").trigger('click');
  },10);

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
		navText : ["<i class='fa fa-caret-left'></i>","<i class='fa fa-caret-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
});

});
</script>
