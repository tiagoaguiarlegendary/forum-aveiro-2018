<?php
/**
 * Template Name: Brand kit
 *
 * Template for brand kit page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper full-wrapper fullHeight">

	<div class="<?php echo esc_html( $container ); ?> fullHeight">

		<div class="row fullHeight">

			<div class="col-12 content-area fullHeight" >

				<main class="site-main fullHeight">

					<div class="row fullHeight">

						<div class="col-12 col-md-2">

							<?php if (get_field('brand_kit_title')) : ?>
								<div class="mb-5">
									<hr class="divider ml-0">
									<h2 class="body-text-15 text-white "><span class="body-text-20 text-white text-fw-black"><?php echo get_field('brand_kit_title');?></span></h2>
								</div>
							<?php endif;?>

							<?php if( have_rows('download_btn') ): ?>

								<?php while( have_rows('download_btn') ): the_row();

									// fields
									$text = get_sub_field('download_btn_text');
									$label = get_sub_field('download_btn_label');
									$file = get_sub_field('download_btn_file');

									?>

									<div>
										<?php if ($text) : ?>
											<div class="__bg-primary-color mb-3">
												<hr class="divider ml-0">
												<p class="body-text-35 text-fw-black primary-color mb-3"><?php echo $text?></p>
											</div>
										<?php endif;?>
										<a download href="<?php echo $file['url']?>" class="btn-download mb-2"><?php echo file_get_contents(get_template_directory() . "/svg/arrow_down.svg");?> <?php echo $label?> </a>
									</div>

									<?php endwhile; ?>

							<?php endif; ?>
						</div>

						<div class="col-12 col-md-10">

						<?php if( have_rows('brand_kit_cta_group') ): ?>
							<div class="row">
								<?php
								while( have_rows('brand_kit_cta_group') ): the_row();
									get_template_part( 'loop-templates/content', 'brand-kit-cta-group' );
								endwhile;
								?>
							</div>
						<?php endif;?>
						</div>

					</div> <!-- .row.fullHeight end -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		//fullContainerPage();
	});
</script>
