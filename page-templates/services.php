<?php
/**
 * Template Name: Services
 *
 * Template for displaying the list of services.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper full-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-12 content-area">

				<main class="site-main fullHeight" role="main">

						<div class="row fullHeight">


							<div class="col-12">
								<div class="row">
									<?php
									$args = array(

										'post_status'       => 'publish',
										'post_type'         => 'services',
										'orderby'						=> 'title',
										'order'							=> 'ASC',
										'posts_per_page' 		=> -1
									);

									args_get_template_part('loop-templates','content-service',$args)
									?>
								</div>
							</div>

						</div>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>

$(function (){
	fullContainerPage('.full-wrapper', true);
});

$('.item-service').on('click',function (){
	$('.item-service').not(this).removeClass('active');

	if ($(this).hasClass('active'))
		$(this).removeClass('active')
	else {
		$(this).addClass('active');
		$('body').animate({
			scrollTop: $(this).offset().top - (isMobile ? 61 : 135)
		}, 750, 'swing');
	}
});
</script>
