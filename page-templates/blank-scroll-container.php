<?php
/**
 * Template Name: Blank with scroll container
 *
 * Template for a blank page with the slit background with a container with scroll
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper wrapper-split bg-split fullHeight">

	<div class="<?php echo esc_html( $container ); ?> fullHeight">

		<div class="row fullHeight">

			<div class="col-12 content-area fullHeight">

				<main class="site-main fullHeight" role="main">
						<div class="content fullHeight custom-scroll">
							<h1 class="info-text-25-uppercase text-center"><?php echo get_field('blank_with_scroll_title'); ?></h1>
							<hr class="divider-primary-color">
							<div class="body-text-18">
								<?php echo get_field('blank_with_scroll_content'); ?>
							</div>
						</div>
				</main><!-- #main -->

				<div class="line translate-center"></div>

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		//fullContainerPage('.wrapper-split');
	});
</script>
