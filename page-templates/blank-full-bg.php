<?php
/**
 * Template Name: Blank full background
 *
 * Template for blank page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper full-wrapper-xs">

	<div class="<?php echo esc_html( $container ); ?> fullHeight">

		<div class="row fullHeight">

			<div class="col-12 col-md-10 ml-auto content-area fullHeight" >

				<main class="site-main ">

					<div class="row">

						<div class="col-12 the-content">
							<?php if (have_posts()) : while (have_posts()) : the_post(); the_content(); endwhile; endif; ?>
						</div>

					</div>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		if(window.innerHeight > 500)
			fullContainerPage('.full-wrapper-xs');
		else
			$('.full-wrapper-xs').height('60vh');
	});
</script>
