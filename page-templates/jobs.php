<?php
/**
 * Template Name: Emprego
 *
 * Template for jobs list.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper list-wrapper">

	<div id="jobs-main">

		<div class="<?php echo esc_html( $container ); ?>">

			<div class="row">

				<div class="col-12 content-area">

					<main class="site-main" role="main">

						<div class="row">

							<div class="col-12 col-md-2">

								<div class="info-box mb-4">
									<h2 class="body-text-15 __lower-lh text-white ">OPORTUNIDADES <br><span class="body-text-20 __lower-lh text-white text-fw-black">DE EMPREGO</span></h2>
								</div>

								<div class="body-text-15 mb-5">
									<p>Preenche o formulário de recrutamento para que possamos entrar em contacto contigo.</p>
								</div>

								<div>
									<a data-view="form" class="btn-default" >candidatura espontânea</a>
								</div>

							</div>

							<div data-view-container="jobs" class="col-12 col-md-10">
								<?php

									$args = array(

										'post_status'       => 'publish',
										'post_type'         => 'jobs',
										'orderby'						=> 'title',
										'order'							=> 'ASC',
										'posts_per_page' 		=> -1

									);

									$the_query = new WP_Query($args);

									if ($the_query->have_posts()) :  $i = 0; ?>

										<div class="jobs-gallery">
											<div class="owl-carousel owl-theme">
												<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

													<div class="item">

														<?php get_template_part( 'loop-templates/content', 'job' ); ?>

													</div>

													<?php $i < 4 ? $i++ : $i = 0 ?>

												<?php endwhile; ?>
											</div>

										</div>

									<?php else: ?>
										<div class="col-12 fullHeight display-flex-center">
											<p class="body-text-30 text-center">De momento nenhuma das nossas lojas está a recrutar. Volta mais tarde.</p>
										</div>
									<?php endif;

									wp_reset_postdata();

									?>

							</div>

							<div data-view-container="form" class="col-12 col-md-9 col-xlg-6 align-self-center">

								<div class="form">
									<div class="form-container">
										<?php echo do_shortcode('[contact-form-7 id="579" title="Formulário de candidatura"]')?>
									</div>
								</div>

							</div>

						</div>

					</main><!-- #main -->

				</div><!-- #primary -->

			</div><!-- .row end -->

		</div><!-- Container end -->

	</div>

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		if (window.innerHeight > 500)
			fullContainerPage('.list-wrapper',true);

		$('a[data-view]').on('click',function (){
			var view = $(this).data('view'),
				$form_container = $('div[data-view-container="form"]'),
				$jobs_container = $('div[data-view-container="jobs"]');

			if (view == 'form'){
				$(this).data('view', 'jobs');
				$(this).html('ver oportunidades' + '<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg");?>');
				$jobs_container.finish().hide("slide", { direction: "left" }, 400, 'easeInOutQuad');
				$form_container.delay(450).show("slide", { direction: "right" }, 400, 'easeInOutQuad');

			}
			else{
				$(this).data('view', 'form');
				$(this).html('candidatura espontânea' + '<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg");?>');
				$jobs_container.delay(450).show("slide", { direction: "left" }, 400, 'easeInOutQuad');
				$form_container.finish().hide("slide", { direction: "right" }, 400, 'easeInOutQuad');

			}
		});

		$('.owl-carousel').owlCarousel({
	    margin:10,
	    responsiveClass:true,
			nav: true,
			dots: false,
			navText : ['',''],
	    responsive:{
	        0:{
	            items:1,
							slideBy: 1
	        },
	        768:{
	            items:2,
							slideBy: 2
	        },
	        992:{
	            items:3,
							slideBy: 3
	        },
					1450:{
	            items:4,
							slideBy: 4
	        }
	    }
		})

	});

</script>
