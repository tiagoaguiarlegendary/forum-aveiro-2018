<?php
/**
 * Template Name: Oportunidades
 *
 * Template for oportunidades page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper single-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-12 hidden-sm-down content-area">

				<main class="site-main" role="main">

						<div class="col-9 ml-auto block-1 nopadding">

							<img class="the_center_img" src="<?php echo $GLOBALS['dir_uri'];?>/img/the_center_img.png" alt="">

							<div class="text-container">
								<p class="secondary-font text-uppercase">Oportunidades de</p>
							</div>

							<div class="links-container ml-auto">
								<ul>
									<li>
										<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'emprego' ) ) ); ?>">emprego<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
									</li>
									<li>
										<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'negócio' ) ) ); ?>">negócio<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
									</li>
								</ul>
							</div>

							<div class="text-container-2 ml-auto">
								<div class="row">
									<div class="text-container-2-second">
										<hr class="divider">
										<p class="body-text-20 text-white">Desenvolva a sua atividade comercial no forum centro e beneficie de um elevado número de potenciais clientes e vendas.</p>
									</div>
								</div>
							</div>

						</div>

				</main><!-- main md content-->

			</div><!-- md content -->

			<div class="col-12 hidden-md-up content-area">

				<main class="site-main" role="main">

					<div class="row">

						<div class="col-12 links-container-xs">
							<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'emprego' ) ) ); ?>">emprego<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
							<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'negócio' ) ) ); ?>">negócio<?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
						</div>


						<div class="col-12 the_center_img">
							<img src="<?php echo $GLOBALS['dir_uri'];?>/img/the_center_img.png" alt="">
						</div>

						<div class="col-12 text-container">
							<div class="text-container-2-second">
								<hr class="divider">
								<p class="body-text-20 text-white">Desenvolva a sua atividade comercial no forum centro e beneficie de um elevado número de potenciais clientes e vendas.</p>
							</div>
						</div>

					</div>

				</main><!-- main xs -->

			</div><!-- xs content -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		if (window.innerHeight > 800)
			fullContainerPage('.single-wrapper');
	});
</script>
