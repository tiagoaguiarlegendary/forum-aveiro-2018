<?php
/**
 * Template Name: Blank
 *
 * Template for blank page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper blank-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-12 content-area">

				<main class="site-main">

					<div class="row">
						<div class="col-12 col-md-2">
							<div class="info-box">
								<h2 class="body-text-20 __lower-lh text-white text-fw-black"><?php the_title();?></h2>
							</div>
						</div>
						<div class="col-12 col-md-8">
							<div class="row">
								<div class="col-12">
									<?php if (have_posts()) : while (have_posts()) : the_post(); the_content(); endwhile; endif; ?>
								</div>
							</div>
						</div>
					</div>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		fullContainerPage('.blank-wrapper',true);
	});
</script>
