<?php
/**
 * Template Name: Blank with form
 *
 * Template for displaying a form page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper full-wrapper">

	<div class="<?php echo esc_html( $container );?>">
			<main class="site-main" role="main">

				<div class="row">

					<div class="col-md-6 content-area" >
										<div >
										<?php if (get_field('form_pag_title')) : ?>
											<h2 class="body-text-20 yellow-heading"><?php echo get_field('form_pag_title');?></h2>
										<?php else : ?>
											<h2 class="body-text-20 yellow-heading "><?php the_title();?></h2>
										<?php endif;?>
										</div>
										<?php if (get_field('form_pag_description')) : ?>

											<div class="mb-5">

												<?php echo get_field('form_pag_description'); ?>

											</div>

										<?php endif;?>


										<div class="body-text-18">

											<?php
											$directions = get_field('contacts_pag_directions');

											if( $directions ): ?>

											<a class="btn-yellow" target="_blank" href="<?php echo $directions; ?>">COMO CHEGAR AQUI</a>

											<?php endif; ?>

										</div>

									</div> <!-- first half -->

									<!-- <div class="form-wrapper">

										<div class="form">
											<div class="form-container">
												<?php //echo do_shortcode('[contact-form-7 id="231" title="Formulário de contacto"]');?>
											</div>
										</div>

									</div> -->





				</div><!-- .row end -->

			</main><!-- #main -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		fullContainerPage('.full-wrapper',true);
	});
</script>
