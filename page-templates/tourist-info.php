<?php
/**
 * Template Name: Tourist Info
 *
 * Template for Tourist Info page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper full-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-12" >

				<main class="site-main">

					<div class="row pos-rel">

						<?php

						//tourist info builder
						if( have_rows('turist_info_language') ):

								$tourist_info = array(
									'flag' => array(),
									'text' => array(),
									'opening_hours' => array(),
									'opening_hours_text' => array(),
									'address' => array(),
									'address_text' => array(),
									'contact' => array(),
									'contact_text' => array()
								);

						    while ( have_rows('turist_info_language') ) : the_row();

						        array_push($tourist_info["flag"],	get_sub_field('turist_info_language_flag')['url']);
										array_push($tourist_info["text"],	get_sub_field('turist_info_language_text'));
										array_push($tourist_info["opening_hours"],	get_sub_field('turist_info_language_opening_hours'));
										array_push($tourist_info["opening_hours_text"],	get_sub_field('turist_info_language_opening_hours_text'));
										array_push($tourist_info["address"],	get_sub_field('turist_info_language_address'));
										array_push($tourist_info["address_text"],	get_sub_field('turist_info_language_address_text'));
										array_push($tourist_info["contact"],	get_sub_field('turist_info_language_contact'));
										array_push($tourist_info["contact_text"],	get_sub_field('turist_info_language_contact_text'));

						    endwhile;

						endif;

						$active = isset( $_GET['country'] ) ? esc_attr( $_GET['country'] ) : '0';

						?>

						<div class="col-12 col-md-10">

							<div class="row">

								<div class="col-12 col-md-5 text-container-float">

									<div class="info-flag mb-5">
										<?php for ($i = 0, $l = sizeof($tourist_info["flag"]); $i < $l; $i++ ):?>
											<a class="flag <?php echo $i == $active ? 'active' : '';?>" href="?country=<?php echo $i?>"><span><?php echo file_get_contents($tourist_info["flag"][$i]);?></span></a>
										<?php endfor; ?>
									</div>

									<div class="info-text">
										<hr class="divider ml-0">

										<h1 class="body-text-35 text-fw-black primary-color mb-3">Forum Aveiro</h1>

										<?php echo $tourist_info["text"][$active]; ?>
									</div>

								</div>

									<div class="col-12 col-md-6 text-container">
										<div class="row">
											<div class="col-12 col-sm-6">
												<div class="row">
													<div class="col-2 col-md-3 info-text-icon">
														<?php echo file_get_contents(get_template_directory() . "/svg/icon_open_hours.svg")?>
													</div>
													<div class="col-10 col-md-9 info-text-text">
														<p class="primary-color text-fw-regular mb-0"><?php echo $tourist_info["opening_hours"][$active]?></p>
														<?php echo $tourist_info["opening_hours_text"][$active]?>
													</div>
												</div>
											</div>
											<div class="col-12 col-sm-6">
												<div class="row">
													<div class="col-2 col-md-3 info-text-icon">
														<?php echo file_get_contents(get_template_directory() . "/svg/icon_address.svg")?>
													</div>
													<div class="col-10 col-md-9 info-text-text">
														<p class="primary-color text-fw-regular mb-0"><?php echo $tourist_info["address"][$active]?></p>
														<?php echo $tourist_info["address_text"][$active]?>
													</div>
												</div>
												<div class="row">
													<div class="col-2 col-md-3 info-text-icon">
														<?php echo file_get_contents(get_template_directory() . "/svg/icon_contacts.svg")?>
													</div>
													<div class="col-10 col-md-9 info-text-text ">
														<p class="primary-color text-fw-regular mb-0"><?php echo $tourist_info["contact"][$active]?></p>
														<?php echo $tourist_info["contact_text"][$active]?>
													</div>
												</div>
											</div>
										</div>
									</div>
							</div>



						</div>

					</div>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		fullContainerPage('.full-wrapper', true);
	});
</script>
