<?php
/**
 * Template Name: Custom Page
 *
 * Create your own style
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper single-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

      <div class="content custom-content">
        <h1><?php the_title(); ?></h1>
        <p>
          <?php if ( have_posts() ) :
              while ( have_posts() ) : the_post();
                  the_content();
                endwhile;
              endif;
               ?>
        </p>

      </div>


    </div><!-- row -->


	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		//fullContainerPage('.single-wrapper');
	});
</script>
