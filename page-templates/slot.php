<?php

/**

 * Template Name: Horarios

 *

 *

 * @package forum

 */



get_header();

$container = get_theme_mod( 'forum_container_type' );

?>



<div class="wrapper wrapper-split bg-split fullHeight">



	<div class="<?php echo esc_html( $container ); ?> fullHeight">



		<div class="row fullHeight">



			<div class="col-12 content-area fullHeight">



				<main class="site-main fullHeight" role="main">

						<div class="content fullHeight container">

							<div class="schedule-container row">
                <?php if(have_rows('schedule_group')):
                  while(have_rows('schedule_group')): the_row();
                  $localidade = get_sub_field('abertura_localidade');
                  $dias = get_sub_field('abertura_dias');
                  $horario = get_sub_field('abertura_horario');
                  $alt_dias = get_sub_field('abertura_dias_2');
                  $alt_horario = get_sub_field('abertura_horario_2');
									$abertura_dias = get_sub_field('abertura_diariamente');
									$abertura_dias_alt = get_sub_field('abertura_horario_alternativo');
                   ?>
                   <div class="col-md-4 main-desc">
                      <h2><?php echo $localidade; ?></h2>
                      <div class="description">
												<?php if($abertura_dias) {?>
	                        <p><?php if(!empty($dias)){ echo $dias;} ?></p>
												<?php } ?>
                        <p><?php if(!empty($horario)){ echo $horario;} ?></p>
                      </div>
											<?php if($abertura_dias_alt){ ?>
                      <div class="description_2">

                        <p><?php if(!empty($alt_dias)){ echo $alt_dias;} ?></p>
                        <p><?php if(!empty($alt_horario)){ echo $alt_horario;} ?></p>
                      </div>
										<?php } ?>
                   </div>
                 <?php
							  	endwhile;
										endif; ?>

              </div>

						</div>

				</main><!-- #main -->



				<div class="line translate-center"></div>



			</div><!-- #primary -->



		</div><!-- .row end -->



	</div><!-- Container end -->



</div><!-- Wrapper end -->



<?php get_footer(); ?>



<script>

	$(function (){

		//fullContainerPage('.wrapper-split');

	});

</script>
