<?php
/**
 * Template Name: Homepage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );

$home_header = get_field('home_bg');
$home_header_mob = get_field('home_bg_img_mobile');

$header_img = get_field('home_bg_img');
$header_video_mp4 = get_field('home_bg_video_mp4');
$header_video_webm = get_field('home_bg_video_webm');

wp_reset_postdata();

?>

<?php if ($home_header == 'home_bg_img') : ?>

	<?php if ( ! wp_is_mobile() ) : ?>

	<header class="home-header hidden-sm-down" style="background-image: url(<?php echo $header_img['url'];?>);height: <?php echo $header_img['height']?>px"></header>

	<?php else : ?>

	<header class="home-header hidden-md-up" style="background-image: url(<?php echo $home_header_mob['url'];?>);height: <?php echo $home_header_mob['height']?>px; max-height: 500px; background-size: contain;"></header>

	<?php endif;?>

<?php else: ?>

<header class="home-header">
	<video id="video-background" muted autoplay loop class="hidden-sm-down">
		<source src="<?php echo $header_video_mp4;?>" type="video/mp4">
		<source src="<?php echo $header_video_webm;?>" type="video/webm">
	</video>

	<a class="hidden-md-up" target="_blank" style="background-image: url('<?php echo get_field('home_bg_img_mobile')['url'];?>')" href="<?php echo $header_video_mp4;?>"><img src="<?php echo get_field('home_bg_img_mobile')['url'];?>" alt=""></a>
</header>

<?php endif; ?>

<div class="bounce"></div>

<div class="wrapper home-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

			<div class="row">

				<div class="col-12 content-area">

					<main class="site-main" role="main">

						<div class="row">

							<?php

							$args = array(
								  'connected_type' 		=> 'destaques-home',
								  'connected_items' 	=> get_queried_object(),
								  'nopaging' 			=> true,
							);

							args_get_template_part('loop-templates','content-highligth',$args)

							?>

						</div>

					</main><!-- #main -->

				</div><!-- #primary -->

			</div><!-- .row end -->

			<div class="row">

				<div class="col-12 content-area text-center">

					<div class="row justify-content-center">
							<a class="see-more" href="<?php echo esc_url( get_post_type_archive_link('highligth')); ?>">
								<div class="col-12">
									<p class="info-text-15-uppercase mb-4">o que está a acontecer</p>
								</div>

								<div class="col-12 arrow-down">
									<?php echo file_get_contents(get_template_directory() . "/svg/arrow_down.svg"); ?>
								</div>
						</a>
					</div>

				</div>

			</div>

		</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>

$('.bounce').on('click', function(){
	$('html, body').animate({
			scrollTop : $('.site-main').offset().top - 105
	}, 800);
});


</script>
