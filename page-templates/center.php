<?php
/**
 * Template Name: Center
 *
 * Template for centro page.
 *
 * @package forum
 */

get_header();
$container = get_theme_mod( 'forum_container_type' );
?>

<div class="wrapper single-wrapper">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-12 hidden-sm-down content-area">

				<main class="site-main" role="main">

						<div class="col-9 ml-auto block-1 nopadding">

							<img class="the_center_img" src="<?php echo $GLOBALS['dir_uri'];?>/img/the_center_img.jpg" alt="">

							<div class="text-container">
								<p>MAIS DO QUE UM<br>CENTRO COMERCIAL,<br>O TEU FORUM AVEIRO.</p>
							</div>

							<div class="links-container ml-auto">
								<ul>
									<li>
										<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'horários' ) ) ); ?>">HORÁRIOS <?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
									</li>
									<li>
										<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'contactos' ) ) ); ?>">CONTACTOS <?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
									</li>
									<li>
										<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'serviços' ) ) ); ?>">SERVIÇOS <?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
									</li>
								</ul>
							</div>

							<div class="text-container-2 ml-auto">
									<div class="divider">
										<?php echo file_get_contents(get_template_directory() . "/svg/white-triangle.svg"); ?>
									</div>
									<p class="body-text-15 text-white">Inaugurado no dia 29 de setembro de 1998 e propriedade da CBRE
														Global Investors, o Forum Aveiro é um centro comercial localizado em
														pleno coração da cidade de Aveiro, galardoado com o Mipim Award 1999
														para o melhor centro comercial da Europa.<br> O Forum Aveiro constitui um
														projeto de desenvolvimento urbano integrado de elevada qualidade,
														com 76 lojas, 17.500m2 de área bruta locável, parque de estacionamento
														coberto, 56 apartamentos, áreas de lazer e jardim suspenso.
														Em 2018, o Forum Aveiro foi distinguido com o prémio Portugal Cinco Estrelas,
														 na categoria de Centros Comerciais de Portugal, ficando no Top 5 da categoria a nível
														nacional e em primeiro lugar no Distrito de Aveiro.
														A quantidade e variedade de lojas e marcas, a localização e estacionamento do centro, bem como os acessos e transportes foram aspetos salientados pelos votantes.
														A arquitetura contemporânea e a natureza ‘outdoor’ foram igualmente alguns dos aspetos mais valorizados do Centro Comercial...
									</p>
							</div>
							<div class="col-6 hidden-sm-down tribal-bg">
									<?php echo file_get_contents(get_template_directory() . "/svg/chess.svg"); ?>
							</div>

						</div>

				</main><!-- main md content-->

			</div><!-- md content -->

			<div class="col-12 hidden-md-up content-area">

				<main class="site-main" role="main">

					<div class="row">

						<div class="col-12 links-container-xs">
							<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'horários' ) ) ); ?>">Horários <?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
							<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'contactos' ) ) ); ?>">Contactos <?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
							<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'serviços' ) ) ); ?>">Serviços <?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg") ?></a>
						</div>


						<div class="col-12 the_center_img">
							<img src="<?php echo $GLOBALS['dir_uri'];?>/img/the_center_img.jpg" alt="">
						</div>

						<div class="col-12 text-container">
							<p class="body-text-20 text-white text-uppercase">MAIS DO QUE UM<br>CENTRO COMERCIAL,<br>O TEU FORUM AVEIRO.</p>
							<hr class="divider">
							<p class="body-text-20 text-white">Inaugurado no dia 29 de setembro de 1998 e propriedade da CBRE
												Global Investors, o Forum Aveiro é um centro comercial localizado em
												pleno coração da cidade de Aveiro, galardoado com o Mipim Award 1999
												para o melhor centro comercial da Europa.<br> O Forum Aveiro constitui um
												projeto de desenvolvimento urbano integrado de elevada qualidade,
												com 76 lojas, 17.500m2 de área bruta locável, parque de estacionamento
												coberto, 56 apartamentos, áreas de lazer e jardim suspenso.
												Em 2018, o Forum Aveiro foi distinguido com o prémio Portugal Cinco Estrelas,
												 na categoria de Centros Comerciais de Portugal, ficando no Top 5 da categoria a nível
												nacional e em primeiro lugar no Distrito de Aveiro.
												A quantidade e variedade de lojas e marcas, a localização e estacionamento do centro, bem como os acessos e transportes foram aspetos salientados pelos votantes.
												A arquitetura contemporânea e a natureza ‘outdoor’ foram igualmente alguns dos aspetos mais valorizados do Centro Comercial...</p>
						</div>

					</div>

				</main><!-- main xs -->

			</div><!-- xs content -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

<script>
	$(function (){
		//fullContainerPage('.single-wrapper');
	});
</script>
