//onfocus/onblur input
$('input, textarea').not('[type="hidden"], [type="submit"]').each(function (index) {
	var p = $(this).attr('placeholder');

	if (p == '' || p == null || typeof p === 'undefined')
		return false;

	if ($(this).attr('aria-required') == 'true')
		p = p + '*';

	$(this).attr('placeholder', p);
	$(this).attr('onfocus', 'this.placeholder = ""');
	$(this).attr('onblur', 'this.placeholder = "' + p + '"');
});

//first option on select
$('select.input-select').each(function () {
	var name = $(this).find('option').eq(0).text() + ($(this).attr('aria-required') == 'true' ? '*' : '');

	$(this).find('option').eq(0).text(name).prop('disabled', true).val('');
});

//prevent double submission on forms
$(document).on('click', '.wpcf7-submit', function (e) {

	var that = $(this);

	var checkState = setInterval(function () {
		var $form = that.parents('.wpcf7-form'),
			$thanksContainer = $form.find('.form-thanks-container');

		if (that.parent().find('.ajax-loader').hasClass('is-active')) {
			e.preventDefault();
			that.val('A enviar...').prop('disabled', true);
			return false;
		} else {
			if ($form.is('.invalid, .sent, .spam')) {
				that.val('Enviar').prop('disabled', false);
				clearInterval(checkState);

				if ($thanksContainer.length > 0 && $form.is('.sent'))
					$form.find('.form-contact').fadeOut(function () {
						$thanksContainer.fadeIn();

						if ($form.parents('.site-footer').length > 0)
							setTimeout(function () {
								$thanksContainer.fadeOut(function () {
									$form.find('.form-contact').fadeIn();
								});
							}, 5000);
					});
			}
		}
	}, 100);

});
