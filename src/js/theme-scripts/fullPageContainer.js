/**
 * Function to make a full page container
 *
 * container @param string jQuery selector - The selector string of container. By default is .full-wrapper.
 * isMin @param boolean - TRUE if it's only the min-height. By default is FALSE.
 *
 */
var hasListener = false;
var fullContainerPage = function (container, isMin, resize) {

	var c = typeof container === 'undefined' ? '.full-wrapper' : container,
		m = typeof isMin === 'undefined' ? false : isMin,
		r = typeof resize === 'undefined' ? true : resize;

	if ($(c).hasClass('fullContainerPage'))
		$(c).removeAttr("style");

	if (isMobile && c != '.full-wrapper-xs' && c != '.custom-scroll')
		return false;

	setTimeout(function () {
		var h = $('.wrapper-footer').hasClass('float-footer') ? window.innerHeight - $('.navbar').innerHeight() : window.innerHeight - $('.navbar').innerHeight() - $('.wrapper-footer').outerHeight();
		$(c).addClass('fullContainerPage');

		if (c == '.custom-scroll') {
			var x = h - parseInt($(c).css('margin-top')) - parseInt($(c).css('margin-bottom')) - parseInt($(c).parents('.wrapper').css('padding-top')) - parseInt($(c).parents('.wrapper').css('padding-bottom')) - $('.btn-load-more').height() //remove the container top and bottom margins and the wrapper top and bottom paddings to the calculated height
			if (x > $(c).next().children().height() || x > $(c).prev().children().height()) h = x
		}

		return m ? $(c).css('min-height', h + 'px') : $(c).innerHeight(h);
	}, 50);

	if (!r) {
		removeListenerFullContainerPage();
		return false
	}

	if (!hasListener) {
		hasListener = true;
		//addListenerFullContainerPage(c, m);
	}
}

var addListenerFullContainerPage = function (container, isMin) {

	window.onresize = function (event) {
		fullContainerPage(container, isMin);
	};

	window.addEventListener("orientationchange", function () {
		fullContainerPage(container, isMin);
	}, false);
}

var removeListenerFullContainerPage = function () {
	window.onresize = function (event) {
		return false;
	};

	window.removeEventListener("orientationchange", function () {
		return false;
	}, false);
}
