var search = function () {

	properties = {
		working: false
	}

	var store = function (s, key) {

		if (s == '') { //prevent search if s is empty
			noResultsStore();
			return false;
		}

		if (properties.working) return false;

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: 'searchStore',
				key: key,
				s: s
			},
			beforeSend: function () {
				properties.working = true;
				$('.loading-square').show();
			},
			success: function (data) {
				var $container = $('#results-info-container');

				if ($('#livesearch-container').is(':visible')) $('#livesearch-container').hide();

				$container.find('article').remove();
				$container.find('.store-no-results').remove();

				if (data != '' || s == '') {
					$container.append(data);
					storeOnMap($(data)[0].id);
				} else
					noResultsStore();


				$('.loading-square').hide();
				properties.working = false;
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log('xhr: ' + xhr, 'textStatus: ' + textStatus, 'errorThrown: ' + errorThrown);
			}
		});
	}

	var storeOnMap = function (id) {

		if (typeof map_data === 'undefined' || activeView == 'directory')
			return false;

		map_data.showLocation(id, 250);

	}

	var noResultsStore = function () {
		var $container = $('#results-info-container');

		$container.find('article').remove();
		$container.find('.store-no-results').remove();
		$container.append('<div class="store-no-results"><p class="body-text-15">Sem resultados.</p><div>');
	}

	return {
		store: store,
		storeOnMap: storeOnMap,
		noResultsStore: noResultsStore
	}
}()
