/**
 * Load more component to load more post type to a specific container
 *
 * @param args (obj) - The query arguments : post_type, order (default: 'date'),
 * orderby (default: 'DESC'), posts_per_page, max_num_pages
 * @param content_template (string) - The loop template of the content.
 * @param main_container (jQuery selector default: #main) - The jQuery selector where to append the result.
 * @param action (string default:#loadmore) - The action name to ajax|php.
 */


var loadmore = function () {

	var properties = {};

	init_load_more = function (args, content_template, main_container, withRow, action) {

		//args
		properties.post_type = args.post_type;
		properties.order = typeof args.order === 'undefined' ? 'date' : args.order;
		properties.orderby = typeof args.orderby === 'undefined' ? 'DESC' : args.orderby;
		properties.meta_query_args = typeof args.meta_query_args === 'undefined' ? '' : args.meta_query_args;
		properties.posts_per_page = Number(args.posts_per_page);
		properties.max_num_pages = Number(args.max_num_pages);

		properties.current_page = 1;

		properties.content_template = content_template;
		properties.main_container = typeof main_container !== 'undefined' ? main_container : '#main';
		properties.withRow = typeof withRow === 'undefined' ? false : withRow;
		properties.action = typeof action !== 'undefined' ? action : 'loadmore';

		_checkMaxPages();
	}

	get_more = function () {
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: properties.action,
				post_type: properties.post_type,
				order: properties.order,
				orderby: properties.orderby,
				posts_per_page: properties.posts_per_page,
				meta_query_args: properties.meta_query_args,
				page: (function () {
					properties.current_page = properties.current_page + 1;
					return properties.current_page
				})(),
				content_template: properties.content_template,
				tax_query: (function () {
					return typeof properties.filter_args === 'undefined' ? false : JSON.stringify(properties.filter_args);
				})()
			},
			beforeSend: function (xhr) {
				animeLoadMore.start();
			},
			success: function (r) {
				setTimeout(function () {
					animeLoadMore.end();

					var $elemToScroll = $(properties.main_container).hasClass('custom-scroll') ? $(properties.main_container) : ($('.scroll-container').length > 0 && $('.scroll-container').hasClass('fullContainerPage') ? $('.scroll-container') : $('html, body')),
						offsetTop = 999999,
						$content = $(r),
						parentN = 2,
						toAppend = properties.main_container;

					if (properties.withRow) {
						toAppend = properties.main_container + ' > .row';
						parentN = 1;
						appendEl = r;
					} else
						appendEl = '<div class="row mb-5">' + r + '<div>'; //wrap the result to a row

					$(toAppend).append(appendEl).fadeIn(function () {

						//get the offsetTop of the result
						for (var i = 0, l = $content.length; i < l; i++) {
							if ($($content[i])[0].nodeName == 'DIV') {
								var thisOffset = parentN == 1 ? $('#' + $($($content[i])[0].firstElementChild)[0].id).parent().position().top : $('#' + $($($content[i])[0].firstElementChild)[0].id).parent().parent().position().top;
								if (thisOffset < offsetTop)
									offsetTop = thisOffset;
							}
						}

						$elemToScroll.animate({
							scrollTop: $elemToScroll.scrollTop() + offsetTop
						}, 750, 'swing');

					});

					_checkMaxPages();

				}, 500); //delay to see the animation.

			},
			error: function (xhr, textStatus, errorThrown) {
				animeLoadMore.end();
				console.log('xhr: ' + xhr, 'textStatus: ' + textStatus, 'errorThrown: ' + errorThrown)
			}
		});
	}

	define_properties = function (max_num_pages, args) {
		properties.current_page = 1;
		properties.max_num_pages = max_num_pages;
		properties.filter_args = args;
	}

	_checkMaxPages = function () {
		return properties.current_page == properties.max_num_pages ? animeLoadMore.hide() : false;
	}

	return {
		init: init_load_more,
		get: get_more,
		define: define_properties
	}

}();

/**
 * Module to animate the loadmore
 */

var animeLoadMore = function () {
	var animationIteration = "animationiteration webkitAnimationIteration mozAnimationIteration oAnimationIteration oanimationiteration",
		transitionEnd = "transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",
		$btnLoadMore = $(".btn-load-more");

	start = function () {
		if ($btnLoadMore.hasClass('active')) {
			end();

			setTimeout(function () {
				start();
			}, 1000);

			return
		}

		$btnLoadMore.addClass("active");
		$btnLoadMore.one(animationIteration);
	}

	end = function () {
		$btnLoadMore.removeClass("active");
		$btnLoadMore.one(transitionEnd);
	}

	hide = function () {
		$btnLoadMore.fadeOut();
	}

	show = function () {
		$btnLoadMore.fadeIn();
	}

	return {
		start: start,
		end: end,
		hide: hide,
		show: show
	}
}();
