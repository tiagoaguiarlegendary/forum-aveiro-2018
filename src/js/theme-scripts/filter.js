var filter = function () {

	var properties = {}

	var def_properties = function (args, handler, container) {
		properties.post_type = args.post_type;
		properties.$handler = $(handler);
		properties.posts_per_page = Number(args.posts_per_page);
		properties.max_num_pages = Number(args.max_num_pages);
		properties.taxonomy = typeof args.taxonomy !== 'undefined' ? args.taxonomy : '';
		properties.field = typeof args.field !== 'undefined' ? args.field : '';
		properties.action = typeof args.action !== 'undefined' ? args.action : 'filter';
		properties.$container = typeof container !== 'undefined' ? $(container) : $('#main');
		properties.appendOn = properties.$container.data('appendOn');
		properties.current_page = 1;

		//attachments
		properties.$handler.on('change', function () {
			_search();
		})

	}


	var _search = function () {
		var value = (function () {
			var v,
				l = properties.$handler.length;
			if (l > 1) {
				v = [];
				for (var i = 0; i < l; i++) {
					if (properties.$handler[i].checked)
						v.push(Number(properties.$handler[i].value));
				}
				v = JSON.stringify(v);
			} else
				v = properties.$handler.val();

			return v;
		})();

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: properties.action,
				post_type: properties.post_type,
				posts_per_page: properties.posts_per_page,
				page: properties.current_page,
				term: value
			},
			beforeSend: function (xhr) {
				properties.$handler.prop('disabled', true);
				$('.loading-square').show();
			},
			success: function (r) {
				var result = JSON.parse(r);

				if (properties.appendOn == '.grid-masonry') {
					if (typeof msnry !== 'undefined') { //if Masonry grid exist
						var $content = $(result.content),
							allItens = document.querySelectorAll('.grid-masonry-item'),
							$appendOn = $(properties.appendOn);

						allItens.forEach(function (v) {
							msnry.remove(v);
						});

						$appendOn.imagesLoaded(function () {
							$appendOn.append($content);
							msnry.appended($content);
							msnry.layout();
						});

					}
				} else
					properties.$container.empty().append('<div class="row">' + result.content + '<div>').fadeIn();

				if (properties.current_page == result.max_num_pages)
					animeLoadMore.hide()
				else {
					animeLoadMore.show();

					//define properties to loadmore module
					loadmore.define(result.max_num_pages, value == '' ? false : {
						taxonomy: properties.taxonomy,
						field: properties.field,
						terms: value
					});
				}

				properties.$handler.prop('disabled', false);
				$('.loading-square').hide();

			},
			error: function (xhr, textStatus, errorThrown) {
				$('.loading-square').hide();
				console.log('xhr: ' + xhr, 'textStatus: ' + textStatus, 'errorThrown: ' + errorThrown)
			}
		});
	}

	return {
		init: def_properties
	}

}();
