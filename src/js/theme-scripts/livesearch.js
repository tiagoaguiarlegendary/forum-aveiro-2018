var liveSearch = function () {

	var properties = {}

	init = function (input_selector) {
		//define properties
		properties.$input = $(input_selector); //input element
		properties.action = properties.$input.data('action'); //action form
		properties.$container = $(properties.$input.data('container')); //container where the result will be display
		properties.item_class = properties.$input.data('item-class'); //the element that will be clicked to be select
		properties.post_type = properties.$input.data('post-type'); //the post type that will be searched
		properties.working = false;
		properties.firstID = '';

		//start listeners
		properties.$input.on('keyup', function (e) {
			var val = properties.$input.val();
			properties.firstID = ''; //reset firstID

			if (e.keyCode == 13) {
				if (properties.working) {
					var queueAjax = setInterval(function () {
						if (properties.working)
							return false;
						else {
							clearInterval(queueAjax);
							_search(val, true);
						}
					}, 50);
				} else
					_search(val, true);
			} else if (properties.$input.val() == '')
				properties.$container.hide();
			else
				_search(val);
		});

		showInfo(true);

		$(document).click(function (e) {
			// if the target of the click isn't the container nor a descendant of the container
			if (!properties.$container.is(e.target) && properties.$container.has(e.target).length === 0)
				if (properties.$container.is(':visible')) properties.$container.hide();
		});

	}

	var showInfo = function (flag) {

		properties.$container.off('click', '.' + properties.item_class); //remove previous listener
		properties.$container.on('click', '.' + properties.item_class, function () {
			var name = $(this).data('storeName'),
				id = $(this).data('storeId');
			properties.$input.val(name);
			properties.$container.hide();

			if (flag) {
				search.store(id, 'store_id');
				search.storeOnMap(id);
			}

		});

	}

	var _search = function (s, goSearch) {
		properties.firstID = ''; //reset firstID

		if (s == '') { //prevent search if s is empty
			search.noResultsStore();
			return false;
		}

		if (properties.working) return false;

		var doSearch = typeof goSearch === 'undefined' ? false : goSearch;

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: properties.action,
				post_type: properties.post_type,
				s: s
			},
			beforeSend: function () {
				properties.working = true;
				$('.loading-square').show();
			},
			success: function (data) {
				var result = JSON.parse(data),
					haveResults = false,
					searchVal = s.toLowerCase(),
					inputVal = properties.$input.val().toLowerCase();

				properties.$container.empty();

				if (result.length <= 0) {
					if (properties.$container.is(':visible')) properties.$container.hide();

					$('.loading-square').hide();
					properties.working = false;
					search.noResultsStore();
					return false;
				}

				for (var i = 0, len = result.length; i < len; i++) {
					if (result[i].title.toLowerCase().match('^' + inputVal.toLowerCase())) { //startsWith replace for match - ie 11 fix. https://stackoverflow.com/questions/3715309/how-to-know-that-a-string-starts-ends-with-a-specific-string-in-jquery
						properties.$container.append('<li><a data-store-name="' + result[i].title + '" data-store-id="' + result[i].store_id + '" class="' + properties.item_class + '">' + result[i].title + '</a></li>');
						haveResults = true;
						if (properties.firstID == '')
							properties.firstID = result[i].store_id;

					}
				}

				if (searchVal != '') {
					if (haveResults)
						properties.$container.show();
					else
						search.noResultsStore();
				}

				$('.loading-square').hide();
				properties.working = false;

				if (doSearch)
					_doShearch(properties.firstID);

			},
			error: function (xhr, textStatus, errorThrown) {
				console.log('xhr: ' + xhr, 'textStatus: ' + textStatus, 'errorThrown: ' + errorThrown);
			}
		});

	}

	var _doShearch = function (id) {
		console.log('id', id);
		properties.firstID = ''; //reset firstID
		search.store(id, 'store_id');
	}

	return {
		init: init,
		showInfo: showInfo
	}

}();
