$(function () {

	if (!cookie.check('cookieConsent')) {
		$('#cookies-warning').show();
		$('#cookies-warning').on('click', 'a[data-set]', function () {
			cookie.set('cookieConsent', $(this).data('set'), 15);
			$('#cookies-warning').fadeOut();
		});
	}


	navbarComponent();

	window.onresize = function (event) {
		navbarComponent();
	};

	//item-highlight hover animation
	$('.item-hover a').hover(function () {
		$(this).parents('.item-hover').addClass('__on-hover');
	}, function () {
		$(this).parents('.item-hover').removeClass('__on-hover');
	});

});
