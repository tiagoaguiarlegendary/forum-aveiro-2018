//Navbar component
var navbarComponent = function () {
	var $navbarDropdown = $('#navbarNavDropdown');

	if (isMobile || window.innerWidth < 1200) {

		$navbarDropdown.on('show.bs.collapse', function () {
			$('.brand-logo').fadeOut(150);
		});

		$navbarDropdown.on('hide.bs.collapse', function () {
			$('.brand-logo').fadeIn(150);
		});

		//change location of form-subscribe container
		$('.form-subscribe').insertAfter($('.form-subscribe').parent().children().eq(0));

		if ($navbarDropdown.hasClass('menu-menu-container'))
			$navbarDropdown.removeClass('menu-menu-container').addClass('collapse navbar-collapse');

		//remove handlers
		$('.navbar-header-menu').off('mouseenter.mouseInteraction');
		$('nav.navbar').off('mouseleave.mouseInteraction');

		$('navbar').removeAttr('style');

		return false;
	}

	$navbarDropdown.removeClass('collapse navbar-collapse').addClass('menu-menu-container');

	var navbarHeight = $('.navbar').innerHeight();

	$('.navbar-header-menu').on('mouseenter.mouseInteraction', '.menu-item', function (e) {
		$('.navbar-nav > .menu-item').fadeIn(1000);
		$('.navbar').height($('#main-menu').height());
	});

	$('nav.navbar').on('mouseleave.mouseInteraction', function (e) {
		$('.navbar-header-menu .menu-item:not(.active):not(.current-menu-ancestor)').fadeOut(250, function () {
			$('.navbar').innerHeight(navbarHeight);
		});
	});

	$('#main-menu > .menu-item.active, #main-menu > .menu-item.current-menu-ancestor').insertBefore($('#main-menu').children().eq(0));

	if ($('#main-menu .menu-item.active').length < 1)
		$('#main-menu').find('.menu-item:first-child').addClass('active').css('text-decoration', 'none');
}
