<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package forum
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'forum_container_type' );
?>

<?php if (get_field('siblings_page_link')) : ?>
	<div class="siblings_page_link">
		<?php
		$page_id = get_field('siblings_page_link_before',false,false);
		if ($page_id):?>
			<a class="before_link" href="<?php echo get_the_permalink($page_id);?>"><?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg");?> <p><?php echo get_the_title($page_id); ?></p></a>
		<?php endif;?>

		<?php
		$page_id = get_field('siblings_page_link_after',false,false);
		if ($page_id):?>
			<a class="after_link" href="<?php echo get_the_permalink($page_id);?>"><?php echo file_get_contents(get_template_directory() . "/svg/arrow_right.svg");?> <p><?php echo get_the_title($page_id); ?></p></a>
		<?php endif;?>
	</div>
<?php endif;?>


<div class="wrapper wrapper-footer __border-bottom <?php echo in_array( $GLOBALS['page_template'] , $GLOBALS['pages_bg_transparent']) ? 'footer-white-links' : '';?>">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer">

					<a target="_blank" href="http://www.cbre.pt/" class="site-footer-logo">
						<span>Gestão:</span>
						<?php echo file_get_contents(get_template_directory() . "/svg/cbre-logo.svg");?>
					</a>

					<div class="row">

						<div class="col-12 col-md-9 col-lg-12 nopadding">

							<?php wp_nav_menu(
								array(
									'theme_location'  => 'footer-menu',
									'container_class' => 'site-footer-container',
									'menu_class'      => 'site-footer-links',
									'menu_id'         => 'footer-links',
									'walker'          => new WP_Bootstrap_Navwalker(),
								)
							); ?>

							<?php
							$form_subscribe = '';//do_shortcode('[contact-form-7 id="213" title="Subscrições a Newsletter"]');

							if ($form_subscribe !== '') : ?>

							<div class="form-subscribe">
								<?php echo $form_subscribe?>
							</div>

							<?php endif;?>

						</div>

						<div class="col-12 col-md-3 nopadding social-links">
							<?php dynamic_sidebar( 'footerlinks' ); ?>
						</div>

					</div>

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page -->

<div id="cookies-warning">
    Para melhorar a sua experiência, usamos cookies próprios e de terceiros. Ao continuar a navegar considera-se que aceita a nossa <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Política de privacidade' ) ) ); ?>">política privacidade</a>.
	<a data-set="yes" title="Aceito">Aceito</a>
	<a data-set="no" title="Rejeitar">Rejeito</a>
</div>

<?php wp_footer(); ?>
</body>

</html>
